### Idea

Idea for a new feature or enhancement here

### Proposal

technical proposal on how to implement that feature (may be rough on the edges, but helpful to get the idea)
