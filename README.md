# Jimmey - A discord bot written in java

Jimmey is first of all a fun project. First kind of features should be some todo, scheduling stuff and some tries to get
the events and tasks into a calendar or as a reminder in discord.

## Build Status
Release on Master (**Version 0.25**):  
[![build status](https://gitlab.com/hadesrofl/Jimmey/badges/master/build.svg)](https://gitlab.com/hadesrofl/Jimmey/commits/master)

Development Branch (going for **Version 0.3**):

[![build status](https://gitlab.com/hadesrofl/Jimmey/badges/development/build.svg)](https://gitlab.com/hadesrofl/Jimmey/commits/development)

## Commands

Below is a list of available commands. Be aware that *~* is the default prefix as mentioned in *config.json*. To switch the default prefix a simple change in config.json is all it needs. You can also call this commands without using a prefix. Then you need to mention the bot e.g. @jimmey help

Following commands are available:

```
~games --> shows a list of your games and your time in game
~help --> prints this help
~permission --> to set permissions for the bot for specific channels
~remind --> to remind you about something
~schedule --> to schedule events
~stats --> shows the stats of this guild
~time --> returns you the current time of Jimmey
~todo --> manages your todo list
all commands have the -help subcommand to check for further details e.g.~schedule -help
```
