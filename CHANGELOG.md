# Jimmey - Changelog

## Version 0.25
* Bugfix in Cleaner that cleaned everything starting with PREFIX
* added support for calling commands via mentions
* added command stats to check stats for the guild the user is logged on
* added feature to track the status of a user and with the game command a user is able to show all games he played with a number of times he was in that game status
* added support to set the avatar of the bot via config file
* refactored the command checker so that it only checks commands. The command itself is another class for a specific command e.g. HelpCommand, ScheduleCommand etc.
* Added Todo Command to add todos to a list and being able to edit and remove them
* Added a sqlite db to persist data like events, permissions for channels, game status and todos
* Added PermissionCommand to set a level of permission to the specific channels e.g. Listen, Reply, Write, None
* ScheduleCommand now handles events, persists them and creates iCal files out of events

## Version 0.2
* added garbage collector for command calls and sent messages by the bot
* improved reminder functionality to use less threads
* added json config file
* fixed issue in remind command
* UTCChecker to verify a given UTC format by the user if it's a correct one
* catch exception due to wrong use of commands and reply with an error message to the user
* help and errors always reply to caller in a dm to lessen the spam in the channel

## Version 0.1
* added basic functionality
* listens to mentions and messages
* has the following commands (*prefix* + command name)
  * help
  * time
  * remind
  * schedule (but not fully implemented yet)
