/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.bot;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import de.hadesrofl.jimmey.commands.CommandChecker;
import de.hadesrofl.jimmey.entities.ChannelEntity;
import de.hadesrofl.jimmey.entities.GuildEntity;
import de.hadesrofl.jimmey.entities.UserEntity;
import de.hadesrofl.jimmey.enums.Permission;
import de.hadesrofl.jimmey.enums.PermissionMap;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import de.hadesrofl.jimmey.task.CleanerTask;
import de.hadesrofl.jimmey.task.ReminderTask;
import de.hadesrofl.jimmey.task.TrackingTask;
import de.hadesrofl.jimmey.utils.PermissionChecker;
import de.hadesrofl.jimmey.utils.database.DBConnection;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.MentionEvent;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.MessageSendEvent;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.StatusChangeEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Status;
import sx.blah.discord.handle.obj.Status.StatusType;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.Image;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * 
 * <p>
 * <strong>last update:</strong> 25.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * Jimmey - the - bot. Handles some basic commands
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class JimmeyBot extends BaseBot {
	/**
	 * Cleaner for sent messages
	 */
	private CleanerTask cleaner;
	/**
	 * Task routine to check for due reminder
	 */
	private ReminderTask reminder;
	/**
	 * Command Checker for this bot
	 */
	private CommandChecker cmdChecker;
	/**
	 * Output path for ICal files created via
	 * {@link de.hadesrofl.jimmey.entities.EventEntity CalEvent}
	 */
	private String iCalOutputPath;
	/**
	 * Task for the tracking command
	 */
	private TrackingTask tracking;
	/**
	 * The connection to the db
	 */
	private DBConnection db;

	/**
	 * Constructor
	 * 
	 * @param client
	 *            is the discord client
	 * @param prefix
	 *            is the prefix for commands used by this bot
	 * @param username
	 *            is the name of this bot
	 * @param timezone
	 *            is the time zone of this bot
	 * @param reminderDelay
	 *            is the delay after which the
	 *            {@link de.hadesrofl.jimmey.task.ReminderTask ReminderTask} is
	 *            started
	 * @param reminderPeriod
	 *            is the period after which the reminder task will6 be run and
	 *            checks for reminders that should be triggered
	 * @param CET
	 *            is the cleaner execution time and therefore a period after
	 *            which the {@link de.hadesrofl.jimmey.task.CleanerTask
	 *            CleanerTask} is called
	 * @param avatarFile
	 *            is the path to the file for the avatar which is set on a
	 *            ReadyEvent
	 * @param iCalOutput
	 *            is the path to the file to save temporarly an ical file
	 *            created by {@link de.hadesrofl.jimmey.entities.EventEntity
	 *            CalEvent}
	 */
	public JimmeyBot(IDiscordClient client, String prefix, String username, String timezone, long reminderDelay,
			long reminderPeriod, long CET, String avatarFile, String iCalOutput) {
		super(client, prefix, username, timezone, avatarFile);
		client.getDispatcher().registerListener(this);
		this.cleaner = new CleanerTask();
		this.cmdChecker = new CommandChecker(this.prefix);
		this.avatarFile = avatarFile;
		Timer t = new Timer();
		this.tracking = new TrackingTask();
		t.schedule(cleaner, 0, CET);
		reminder = new ReminderTask(client);
		t.schedule(reminder, reminderDelay, reminderPeriod);
		this.iCalOutputPath = iCalOutput;
		this.db = DBConnection.getInstance();
	}

	/**
	 * Updates the DB on the start of this bot
	 * 
	 * @param event
	 *            is the ReadyEvent used to determine the connection to all
	 *            guilds
	 */
	private void updateDB(ReadyEvent event) {
		Map<String, Boolean> guilds = new HashMap<String, Boolean>();
		Map<String, Boolean> channels = new HashMap<String, Boolean>();
		Map<String, Boolean> users = new HashMap<String, Boolean>();
		if (event != null && event.getClient() != null && event.getClient().getGuilds().size() > 0) {
			for (IGuild g : event.getClient().getGuilds()) {
				updateGuild(g);
				guilds.put(g.getID(), true);
				for (IChannel c : g.getChannels()) {
					updateChannel(c, g);
					channels.put(c.getID(), true);
				}
				for (IUser u : g.getUsers()) {
					updateUser(u, g);
					users.put(u.getID(), true);
				}
				updateUser(this.client.getOurUser(), g);
			}
			List<GuildEntity> guildResults = db.getQm().findAllGuilds();
			if (guildResults != null) {
				for (GuildEntity g : guildResults) {
					if (g != null && !guilds.containsKey(g.getId())) {
						db.getQm().deleteGuild(g.getId());
					}
				}
				List<ChannelEntity> channelResults = db.getQm().findAllChannels();
				if (channelResults != null) {
					for (ChannelEntity c : channelResults) {
						if (c != null && !channels.containsKey(c.getId())) {
							db.getQm().deleteChannel(c.getId());
						}
					}
				}
				List<UserEntity> userResults = db.getQm().findAllUsers();
				if (userResults != null) {
					for (UserEntity u : userResults) {
						if (!users.containsKey(u.getId())) {
							db.getQm().deleteUser(u.getId());
						}
					}
				}
			}
		}
	}

	/**
	 * Updates the db regarding a given guild
	 * 
	 * @param g
	 *            is the IGuild Object to update in the db
	 */
	private void updateGuild(IGuild g) {
		GuildEntity guild = db.getQm().findGuild(g.getID());
		if (guild == null) {
			db.getQm().insertGuild(g.getID(), g.getName());
		} else {
			db.getQm().updateGuild(g.getID(), g.getName());
		}
	}

	/**
	 * Updates the db regarding a given channel
	 * 
	 * @param c
	 *            is the IChannel Object to update in the db
	 * @param g
	 *            is the IGuild Object of the channel
	 */
	private void updateChannel(IChannel c, IGuild g) {
		ChannelEntity channel = db.getQm().findChannel(c.getID(), g.getID());
		if (channel == null) {
			db.getQm().insertChannel(c.getID(), c.getName(), g.getID(),
					PermissionMap.getPermissionValue(Permission.WRITE));
		} else {
			int permission;
			if (channel.getPermission() == null) {
				permission = PermissionMap.getPermissionValue(Permission.WRITE);
			} else {
				permission = PermissionMap.getPermissionValue(channel.getPermission());
			}
			db.getQm().updateChannel(c.getID(), c.getName(), g.getID(), permission);
		}
	}

	/**
	 * Updates the db regarding a user
	 * 
	 * @param u
	 *            is the IUser Object to update in the db
	 * @param g
	 *            is the IGuild Object of the user
	 */
	private void updateUser(IUser u, IGuild g) {
		UserEntity user = db.getQm().findUser(u.getID());
		if (user == null) {
			db.getQm().insertUser(u.getID(), u.getName(), g.getID());
		} else {
			db.getQm().updateUser(u.getID(), u.getName());
		}
	}

	/**
	 * Listens to the ReadyEvent and reacts on that event
	 * 
	 * @param event
	 *            is the ReadyEvent
	 */
	@EventSubscriber
	public void onReady(ReadyEvent event) {
		try {
			client.changeUsername(username);
			client.changeAvatar(Image.forFile(new File(avatarFile)));
			client.changePresence(false);
			client.changeStatus(Status.game(prefix + "help for infos"));
			updateDB(event);
		} catch (DiscordException | RateLimitException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Listens to a mention. TODO: Needs a better reply on a mention (wip)
	 * 
	 * @param event
	 *            is a MentionEvent
	 */
	@EventSubscriber
	public void onMentionEvent(MentionEvent event) {
		if (PermissionChecker.checkPermission(event.getMessage().getChannel().getID(),
				event.getMessage().getGuild().getID(), Permission.LISTEN) && !event.getMessage().mentionsEveryone()) {
			try {
				if (cmdChecker.checkCommands(event.getMessage(), true) == ProcessStateEnum.NOT_PROCESSED) {
					if (PermissionChecker.checkPermission(event.getMessage().getChannel().getID(),
							event.getMessage().getGuild().getID(), Permission.WRITE)) {
						event.getMessage().getChannel().sendMessage("You don't want to order me around? So whaddup?");
					}
				} else {
					this.cleaner.addMessage(event.getMessage());
				}
			} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
				System.err.println(e.getMessage());
			}
		}
	}

	/**
	 * Method called upon receiving a message. This is where Jimmey listens to
	 * commands
	 * 
	 * @param event
	 *            is the MessageReceivedEvent
	 */
	@EventSubscriber
	public void onMessageReceivedEvent(MessageReceivedEvent event) {
		if (PermissionChecker.checkPermission(event.getMessage().getChannel().getID(),
				event.getMessage().getGuild().getID(), Permission.LISTEN)
				&& event.getMessage().getContent().startsWith(prefix)) {
			if (cmdChecker.checkCommands(event.getMessage(), false) != ProcessStateEnum.NOT_PROCESSED
					&& !event.getMessage().getChannel().isPrivate()) {
				this.cleaner.addMessage(event.getMessage());
			}

		}
	}

	/**
	 * Method called upon a message sent by this bot.
	 * 
	 * @param event
	 *            is the MessageSendEvent
	 */
	@EventSubscriber
	public void onMessageSend(MessageSendEvent event) {
		if (!event.getMessage().getChannel().isPrivate() && !event.getMessage().getContent().startsWith("Stats")) {
			this.cleaner.addMessage(event.getMessage());
		}
	}

	/**
	 * Method called upon receiving a change on a status of an user
	 * 
	 * @param event
	 *            is the StatusChangeEvent
	 */
	@EventSubscriber
	public void onStatusChange(StatusChangeEvent event) {
		if (event.getUser() == this.client.getOurUser() && event.getUser().isBot()) {
			return;
		}
		if (event.getNewStatus().getType() == StatusType.GAME) {
			tracking.addStatus(event, System.currentTimeMillis());
		}
		if (event.getOldStatus().getType() == StatusType.GAME) {
			tracking.processStatus(event, System.currentTimeMillis());
		}
	}

	/**
	 * Gets the reminder
	 * 
	 * @return the reminder as ReminderTask
	 */
	public ReminderTask getReminder() {
		return reminder;
	}

	/**
	 * Gets the iCalOutput
	 * 
	 * @return the iCalOutput as String
	 */
	public String getiCalOutput() {
		return iCalOutputPath;
	}
}
