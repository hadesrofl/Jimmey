/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.bot;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.util.DiscordException;

/**
 * 
 * <p>
 * <strong>last update:</strong> 16.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * A basic bot which just logs in to a client
 * </p>
 *
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.21
 */
public class BaseBot {
	/**
	 * Instance of the discord client
	 */
	protected IDiscordClient client;
	/**
	 * Prefix for commands
	 */
	protected String prefix;
	/**
	 * Name of Bot
	 */
	protected String username;
	/**
	 * Time zone of this bot
	 */
	protected String timezone;
	/**
	 * File for the avatar
	 */
	protected String avatarFile;

	/**
	 * Constructor
	 * 
	 * @param client
	 *            is the discord client
	 * @param prefix
	 *            is the prefix for commands used by this bot
	 * @param username
	 *            is the name of this bot
	 * @param timezone
	 *            is the time zone of this bot
	 * @param avatarFile
	 *            is the file of the avatar to set on a ReadyEvent
	 */
	public BaseBot(IDiscordClient client, String prefix, String username, String timezone, String avatarFile) {
		this.client = client;
		this.prefix = prefix;
		this.username = username;
		this.timezone = timezone;
		this.avatarFile = avatarFile;
	}

	/**
	 * Logs in to a client with a given token
	 * 
	 * @param token
	 *            is the token of the bot
	 * @return a client this app is logged in to or null if logging resulted in
	 *         an error
	 */
	public static IDiscordClient login(String token) {
		ClientBuilder builder = new ClientBuilder();
		builder.withToken(token);
		IDiscordClient client = null;
		try {
			client = builder.login();
		} catch (DiscordException e) {
			System.err.println(e.getMessage());
		}
		return client;
	}

	/**
	 * Gets the client
	 * 
	 * @return the client as IDiscordClient
	 */
	public IDiscordClient getClient() {
		return client;
	}

	/**
	 * Gets the prefix
	 * 
	 * @return the prefix as String
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * Gets the username
	 * 
	 * @return the username as String
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Gets the timeZone
	 * 
	 * @return the timeZone as String
	 */
	public String getTimeZone() {
		return timezone;
	}
}