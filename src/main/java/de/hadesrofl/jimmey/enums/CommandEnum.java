/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.enums;

import de.hadesrofl.jimmey.BotLauncher;
import de.hadesrofl.jimmey.commands.BaseCommand;
import de.hadesrofl.jimmey.commands.HelpCommand;
import de.hadesrofl.jimmey.commands.PermissionCommand;
import de.hadesrofl.jimmey.commands.RemindCommand;
import de.hadesrofl.jimmey.commands.ScheduleCommand;
import de.hadesrofl.jimmey.commands.StatCommand;
import de.hadesrofl.jimmey.commands.TimeCommand;
import de.hadesrofl.jimmey.commands.TodoCommand;
import de.hadesrofl.jimmey.commands.GameCommand;
import sx.blah.discord.handle.obj.IMessage;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This enum handles all commands that are possibly supported by the bot
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public enum CommandEnum {
	// @formatter:off
	GAME(new GameCommand("games", BotLauncher.INSTANCE)),
	HELP(new HelpCommand("help", BotLauncher.INSTANCE)),
	PERMISSION(new PermissionCommand("permission", BotLauncher.INSTANCE)),
	REMIND(new RemindCommand("remind", BotLauncher.INSTANCE)),
	SCHEDULE(new ScheduleCommand("schedule", BotLauncher.INSTANCE)),
	STATS(new StatCommand("stats", BotLauncher.INSTANCE)),
	TIME(new TimeCommand("time", BotLauncher.INSTANCE)),
	TODO(new TodoCommand("todo", BotLauncher.INSTANCE));
	// @formatter:on

	/**
	 * Base Command of this enum
	 */
	private BaseCommand command;

	/**
	 * Constructor
	 * 
	 * @param bot
	 *            is the instance of this bot
	 */
	private CommandEnum(BaseCommand command) {
		this.command = command;
	}

	/**
	 * Processes a message and content according to the command. Returns true or
	 * false depending on it's success
	 * 
	 * @param message
	 *            is the IMessage to process
	 * @param filteredContent
	 *            is the content to process
	 * @return true if the processing was successful, otherwise false
	 */
	public ProcessStateEnum process(IMessage message, String filteredContent) {
		return this.command.process(message, filteredContent);
	}

	/**
	 * Gets the keyword of this command
	 * 
	 * @return the keyword as string
	 */
	public String getKeyword() {
		return this.command.getKeyword();
	}

	/**
	 * Gets the command class of this enum
	 * 
	 * @return the command class of this enum
	 */
	public BaseCommand getCommand() {
		return command;
	}

	/**
	 * For testing purposes we need to reset the bot reference without using the
	 * main method
	 */
	public void updateBot() {
		command.updateBotReference();
	}
}
