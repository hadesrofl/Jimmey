/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.enums;

/**
 * 
 * <p>
 * <strong>last update:</strong> 13.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * An Enum containing all UTC formats and their principal cities and countries
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.2
 */
public enum UTC {
	// @formatter:off
	minus1200("-12:00", "Baker Island", "Howland Island"), 
	minus1100("-11:00", "United States/American Samoa", "United States/Jarvis Island",
			"United States/Kingman Reef", "United States/Midway Atoll", "United States/Plamyra Atoll", "New Zealand/Niue"), 
	minus1000("-10:00", "Papeete", "Honolulu", "France/French Polynesia", "New Zealand/Cook Islands", 
			"United States/Aleutian Islands", "United States/Hawaii", "United States/Johnston Atoll"), 
	minus0930("-09:30", "France/Marquesas Islands"), 
	minus0900("-09:00", "Anchorage", "France/Gambier Islands", "United States/Alaska"), 
	minus0800("-08:00", "Los Angeles", "Vancouver", "Tijuana","Canada/British Columbia", "Canada/Yukon", "France/Clipperton Island", 
			"Mexico/Baja California", "United Kingdom/Pitcairn Islands", "United States/California", "United States/Salmon River",
			"United States/Nevada", "United States/Oregon", "United States/Washington"),
	minus0700("-07:00", "Phoenix", "Calgary", "Ciudad Juarez", "Canada/Alberta", "Canada/British Columbia",
			"Canada/Northwest Territories",	"Canada/Nunavut", "Canada/Saskatchewan", "Mexico/Baja California Sur", 
			"Mexico/Chihuahua", "Mexico/Nayarit", "Mexico/Sinaloa", "Mexico/Sonora", "United States/Arizona",
			"United States/Colorado", "United States/Idaho", "United States/Kansas", "United States/Wallace", 
			"United States/Montana", "United States/Nebraska", "United States/Nevada", "United States/New Mexico",
			"United States/North Dakota", "United States/Oregon", "United States/South Dakota", "United States/Texas", 
			"United States/Utah", "United States/Wyoming"),
	minus0600("-06:00", "Chicago", "Guatemala City", "Mexico City", "San José", "San Salvador", "Winnipeg", "Belize", 
			"Canada/Manitoba","Canada/Nunavut", "Canada/Onatario", "Canada/Saskatchewan", "Chile", "Costa Rica", 
			"Ecuador/Galápagos Islands", "El Salvador", "Guatemala", "Honduras", "Mexico", "Nicaragua", "United States/Alabama",
			"United States/Arkansas", "United States/Florida", "United States/Illinois", "United States/Indiana",
			"United States/Iowa", "United States/Kansas", "United States/Kentucky", "United States/Louisiana", 
			"United States/Michigan", "United States/Minnesota", "United States/Mississippi", "United States/Missouri",
			"United States/Nebraska", "United States/North Dakota", "United States/Oklahoma", "United States/South Dakota",
			"United States/Tennessee", "United States/Texas", "United States/Wisconsin"),
	minus0500("-05:00", "New York", "Lima", "Toronto", "Bogotá", "Havana", "Kingston", "Bahamas", "Brazil/Acre", "Brazil/Amazonas",
			"Canada/Nunavut", "Canada/Ontario", "Canada/Quebec", "Colombia", "Cuba", "Ecuador", "Haiti", "Jamaica", 
			"Mexico/Quintana Roo", "Panama", "Peru", "United Kingdom/Cayman Islands", "United States/Delaware", 
			"United States/District of Columbia", "United States/Florida", "United States/Georgia", "United States/Indiana",
			"United States/Kentucky", "United States/Maryland", "United States/Michigan", "United States/New England",
			"United States/New Jersey", "United States/New York", "United States/North Carolina", "United States/Ohio",
			"United States/Pennsylvania", "United States/South Carolina", "United States/Tennessee", "United States/Virginia",
			"United States/West Virginia", "United States/Navassa Island"),
	minus0400("-04:00", "Santiago", "La Paz", "Manaus", "Halifax", "Antigua and Barbuda", "Barbados", "Bolivia", "Brazil/Amazonas",
			"Canada/New Brunswick", "Canada/Newfoundland and Labrador", "Canada/Nova Scotia", "Canada/Prince Edward Island", 
			"Canada/Quebec", "Chile", "Denmark/Greenland", "Dominicia", "Dominican Republic", "France/Guadeloupe", 
			"France/Martinique", "France/Saint Barthélemy", "France/Saint-Martin", "Grenada", "Guyana", "Netherlands/Aruba",
			"Netherlands/Carribean Netherlands", "Netherlands/Curacao", "Netherlands/Sint Maarten", "Paraguay", 
			"Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Trinidad and Tobago", 
			"United Kingdom/Anguilla", "United Kingdom/Bermuda", "United Kingdom/British Virgin Islands", 
			"United Kingdom/Montserrat", "United Kingdom/Turks and Caicos Islands", "United States/Puerto Rico", 
			"United States/U.S. Virgin Islands", "Venezuela"),
	minus0330("-03:30", "St. John's", "Canada/Newfoundland and Labrador"),
	minus0300("-03:00", "Buenos Aires", "Montevido", "São Paulo", "Argentina","Brazil", "Denmark/Greenland", "France/French Guiana",
			"France/Saint-Pierre and Miquelon", "Suriname", "United Kingdom/Falkland Islands", "Uruguay"),
	minus0200("-02:00", "Brazil/Fernando de Noronha", "United Kingdom/South Georgia and the South Sandwich Islands"),
	minus0100("-01:00", "Cape Verde", "Denmark/Greenland", "Portugal/Azores Islands"),
	minus0000("-00:00", "Accra", "Casablanca", "Dakar", "Dublin", "Lisbon", "London", "Burkina Faso", "Côte d'Ivoire",
			"Denmark/Faroe Islands", "Denmark/Greenland", "Gambia", "Ghana", "Guinea", "Guinea-Bissau", "Iceland", 
			"Republic of Ireland", "Liberia", "Mali", "Mauritania", "Morocco", "Portugal", "Sahrawi Republic", 
			"São Tomé and Príncipe", "Spain/Canary Islands", "Senegal", "Sierra Leone", "Togo", "United Kingdom"),
	plus0000("+00:00", "Accra", "Casablanca", "Dakar", "Dublin", "Lisbon", "London", "Burkina Faso", "Côte d'Ivoire",
			"Denmark/Faroe Islands", "Denmark/Greenland", "Gambia", "Ghana", "Guinea", "Guinea-Bissau", "Iceland", 
			"Republic of Ireland", "Liberia", "Mali", "Mauritania", "Morocco", "Portugal", "Sahrawi Republic", 
			"São Tomé and Príncipe", "Spain/Canary Islands", "Senegal", "Sierra Leone", "Togo", "United Kingdom"),
	plus0100("+01:00","Berlin", "Lagos", "Madrid", "Paris", "Rome", "Tunis", "Vienna", "Warsaw", "Albania", "Algeria", "Andorra",
			"Angola", "Austria", "Belgium", "Benin", "Bosnia and Herzegovina", "Cameroon", "Central African Republic",
			"Chad", "Republic of the Congo", "Democratic Republic of the Congo", "Croatia", "Czech Republic", "Denmark", 
			"Equatorial Guinea", "France", "Gabon", "Germany", "Hungary", "Italy", "Kosovo", "Liechtenstein", "Luxembourg",
			"Republic of Macedonia", "Malta", "Monaco", "Montenegro", "Namibia", "Netherlands", "Niger", "Nigeria", "Norway",
			"Poland", "San Marino", "Serbia", "Slovakia", "Slovenia", "Spain", "Sweden", "Switzerland", "Tunisia", 
			"United Kingdom", "Gibraltar", "Vatican City"),
	plus0200("+02:00", "Athens", "Bucharest", "Cairo", "Helsinki", "Jerusalem", "Johannesburg", "Botswana", "Bulgaria", "Burundi",
			"Cyprus", "Democratic Republic of the Congo", "Egypt", "Estonia", "Finland", "Greece", "Israel", "Jordan", "Latvia",
			"Lebanon", "Lesotho", "Lithuania", "Libya", "Malawi", "Moldova", "Mozambique", "State of Palestine", "Romania", 
			"Russia/Nothwestern Federal District", "Rwanda", "South Africa", "Swaziland", "Syria", "Ukraine", 
			"United Kingdom/Akrotiri and Dhekelia", "Zambia", "Zimbabwe"),
	plus0300("+03:00", "Istanbul", "Moscow", "Nairobi", "Baghdad", "Doha", "Khartoum", "Minsk", "Riyadh", "Bahrain", "Belarus",
			"Comoros", "Cyprus/Northern Cyprus", "Democratic Republic of the Congo", "Djibouti", "Eritrea", "Ethiopia",
			"France/French Southern and Antarctic Lands", "France/Mayotte", "Georgia/Abkhazia", "Georgia/South Ossetia", 
			"Iraq", "Kenya", "Kuwait", "Madagascar", "Qatar", "Russia/Central Federal District", 
			"Russia/North Caucasian Federal District", "Russia/Northwestern Federal District", 
			"Russia/Southern Federal District", "Russia/Volga Federal District", "Saudi Arabia", "Somalia", 
			"South Africa/Prince Edward Islands", "South Sudan", "Sudan", "Tanzania", "Turkey", "Uganda", "Ukraine",
			"Yemen"),
	plus0330("+03:30", "Tehran", "Iran"),
	plus0400("+04:00", "Baku", "Dubai", "Samara", "Muscat", "Armenia", "Azerbaijan", "France/French Southern and Antarctic Lands",
			"France/Réunion", "Georgia", "Mauritius", "Oman", "Russia/Volga Federal District", "Seychelles", 
			"United Arab Emirates"),
	plus0430("+04:30", "Kabul", "Afghanistan"),
	plus0500("+05:00", "Karachi", "Tashkent", "Yekaterinburg", "Australia/Heard Island and McDonald Islands", 
			"France/French Southern and Antarctic Lands", "Kazakhstan", "Maldives", "Pakistan", "Russia/Ural Federal District",
			"Russia/Volga Federal District", "Tajikistan", "Turkmenistan", "Uzbekistan"),
	plus0530("+05:30", "Delhi", "Colombo", "India", "Sri Lanka"),
	plus0545("+05:45", "Kathmandu", "Nepal"),
	plus0600("+06:00", "Almaty", "Dhaka", "Omsk", "Bangladesh", "Bhutan", "Kazakhstan", "Kyrgyzstan", 
			"Russia/Siberian Federal District", "United Kingdom/British Indian Ocean Territory"),
	plus0630("+06:30", "Yangon", "Australia/Cocos (Keeling) Islands", "Myanmar"),
	plus0700("+07:00", "Jakarta", "Bangkok", "Krasnoyarsk", "Hanoi", "Hovd", "Vientiane", "Phnom", "Penh",
			"Australia/Christmas Island", "Cambodia", "Indonesia/Java", "Indonesia/Sumatra", "Indonesia/Riau Islands",
			"Indonesia/West Kalimantan", "Indonesia/Central Kalimantan", "Laos", "Mongolia", "Russia/Siberian Federal District",
			"Thailand", "Vietnam"),
	plus0800("+08:00", "Beijing", "Taipei", "Singapore", "Kuala Lumpur", "Bandar Seri Begawan", "Perth", "Manila", 
			"Denpasar Irkutsk", "Ulaanbaatar", "Australia/Western Australia", "Brunei", "China", "Indonesia/North Kalimantan",
			"Indonesia/East Kalimantan", "Indonesia/South Kalimantan", "Indonesia/Lesser Sunda Islands", "Indonesia/Sulawesi",
			"Malaysia", "Mongolia", "Philippines", "Russia/Siberian Federal District", "Singapore", "Taiwan"),
	plus0830("+08:30", "Pyongyang", "North Korea"),
	plus0845("+08:45", "Australia/Western Australia"),
	plus0900("+09:00", "Seoul", "Tokyo", "Ambon", "Yakutsk", "Dili", "Indonesia/Maluku Islands", "Indonesia/Papua", 
			"Indonesia/West Papua", "Japan", "South Korea", "Palau", "Russia/Far Eastern Federal District", "Timor-Leste"),
	plus0930("+09:30", "Adelaide", "Australia/Northern Territory", "Australia/South Australia"),
	plus1000("+10:00", "Brisbane", "Sydney", "Melbourne", "Canberra", "Vladivostok", "Port Moresby", 
			"Australia/Australian Capital Territory", "Australia/New South Wales", "Australia/Queensland", "Australia/Tasmania",
			"Australia/Victoria", "Federated States of Micronesia/Chuuk State", "Federated States of Micronesia/Yap State",
			"Papua New Guinea", "Russia/Far Eastern Federal District", "United States/Guam", 
			"United States/Northern Mariana Islands"),
	plus1030("+10:30", "Australia/New South Wales"),
	plus1100("+11:00", "Noumea", "Australia/Norfolk Island", "Federated States of Micronesia/Kosrae State", 
			"Federated States of Micronesia/Pohnpei State", "France/New Caledonia", 
			"Papua New Guinea/Autonomous Region of Bougainville", "Russia/Far Eastern Federal District", "Solomon Islands",
			"Vanuatu"),
	plus1200("+12:00", "Auckland", "Suva", "France/Wallis and Futuna", "Fiji", "Kiribati/Gilbert Islands", "Marshall Islands",
			"Nauru", "New Zealand", "Russia/Far Eastern Federal District", "Tuvalu", "United States/Wake Island"),
	plus1245("+12:45", "New Zealand/Chatham Islands"),
	plus1300("+13:00", "Kiribati/Phoenix Islands", "New Zealand/Tokelau", "Samoa", "Tonga"),
	plus1400("+14:00", "Kiribati/Line Islands");
	// formatter:on
	/**
	 * Format of the UTC e.g. +01:00
	 */
	private String format;
	/**
	 * Array of principal cities and countries that are effected by this time zone
	 */
	private String[] countries;
	/**
	 * Constructor
	 * @param format is the format of the utc e.g. +01:00
	 * @param countries is an array of principal cities and countries that are effected by this time zone
	 */
	private UTC(
	String format, String...countries){
		this.format = format;
		this.countries = countries;
	}
	/**
	 * Gets the format of the utc
	 * @return a string representing the utc format
	 */
	public String getFormat(){
		return this.format;
	}
	/**
	 * Gets the array of principal cities and countries that are affected by this utc
	 * @return an array of cities and countries
	 */
	public String[] getCountries(){
		return this.countries;
	}
	/**
	 * Represents this UTC as a String
	 * @return a string of this UTC
	 */
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("UTC " + this.format + " affects the following cities/countries:\n");
		int words = 0;
		for(String s : this.countries){
			words++;
			if(words % 4 == 0){
				sb.append("\n");
			}
			sb.append(s + ", ");
		}
		return sb.toString();
	}

}
