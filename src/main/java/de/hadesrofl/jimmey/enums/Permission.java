/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.enums;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class represents enums for different degrees of permission levels for
 * the bot and each channel. The permissions are set in the database and
 * therefore read out of it
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public enum Permission {
	NONE("None"), LISTEN("Listen"), REPLY("Reply"),WRITE("Write") ;
	/**
	 * A description of this Permission
	 */
	private String desc;

	/**
	 * Constructor
	 * 
	 * @param desc
	 *            is the description for this enum
	 */
	private Permission(String desc) {
		this.desc = desc;
	}

	/**
	 * Returns this permission as a string
	 * 
	 * @return this permission as a string
	 */
	public String toString() {
		return desc;
	}
}