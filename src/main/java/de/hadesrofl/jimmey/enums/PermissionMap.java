package de.hadesrofl.jimmey.enums;

/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.EnumMap;
import java.util.Map;

/**
 * 
 * <p>
 * <strong>last update:</strong> 25.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class is a map for {@link Permission} to handle communication of
 * permissions for channels to the database which just accepts and returns
 * integer values
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public final class PermissionMap {
	/**
	 * Map of permissions and their values used for communicating permissions to
	 * the database as it just handles integer
	 */
	private static Map<Permission, Integer> map;
	/**
	 * Is the PermissionMap setup?
	 */
	private static boolean constructed;

	/**
	 * Constructor
	 */
	private static void construct() {
		map = new EnumMap<Permission, Integer>(Permission.class);
		map.put(Permission.NONE, 0);
		map.put(Permission.LISTEN, 1);
		map.put(Permission.REPLY, 2);
		map.put(Permission.WRITE, 3);
		constructed = true;
	}

	/**
	 * Gets the value behind a {@link Permission}.
	 * 
	 * @param p
	 *            is the Permission
	 * @return on success the value of the permission, otherwise -1 to indicate
	 *         an error
	 */
	public static int getPermissionValue(Permission p) {
		if (!constructed) {
			construct();
		}
		if (map.containsKey(p)) {
			return map.get(p);
		} else {
			return -1;
		}
	}

	/**
	 * Gets the permission based on a value
	 * 
	 * @param i
	 *            is the value used to search the permission
	 * @return the permission or null if the permission wasn't found
	 */
	public static Permission getPermission(int i) {
		if (!constructed) {
			construct();
		}
		if (map.containsValue(i)) {
			for (Permission p : map.keySet()) {
				if (map.get(p) == i) {
					return p;
				}
			}
			return null;
		} else {
			return null;
		}
	}
}
