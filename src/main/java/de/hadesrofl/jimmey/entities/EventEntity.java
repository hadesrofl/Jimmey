/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.entities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

import de.hadesrofl.jimmey.BotLauncher;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.util.UidGenerator;
import net.fortuna.ical4j.validate.ValidationException;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * A class to represent a calendar event and print it to a iCal file
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class EventEntity extends BaseEntity {
	/**
	 * Name of the event
	 */
	private String topic;
	/**
	 * Start time and date of the event
	 */
	private DateTime start;
	/**
	 * Temporary Calendar object for creating the event
	 */
	private Calendar startDate;
	/**
	 * Temporary Calendar object for creating the event
	 */
	private Calendar endDate;
	/**
	 * End time and date of the event
	 */
	private DateTime end;
	/**
	 * Id of the user of this event
	 */
	private String userId;
	/**
	 * Output file for the event
	 */
	private File iCalFile;
	/**
	 * Attendees of the event
	 */
	private String participants;
	/**
	 * path to the directory where the output file will be created
	 */
	private String outputPath = BotLauncher.INSTANCE.getiCalOutput();

	/**
	 * Constructor
	 * 
	 * @param userId
	 *            is the id of the user of this event
	 * @param topic
	 *            is the name of the event
	 * @param date
	 *            is the date of the event
	 * @param time
	 *            is the time of the event
	 * @param participants
	 *            is an array of attendees for this event
	 */
	public EventEntity(String userId, String topic, String date, String time, String... participants) {
		super(0 + "");
		this.topic = topic;
		this.participants = getParticipantString(participants);
		this.userId = userId;
		this.startDate = getDate(date, time);
		this.endDate = startDate;
	}

	/**
	 * Constructor used for data from the database
	 * 
	 * @param id
	 *            is the id of the event
	 * @param userId
	 *            is the id of the user of this event
	 * @param topic
	 *            is the topic of this event
	 * @param startDate
	 *            is the start date of this event
	 * @param endDate
	 *            is the end date of this event
	 * @param participants
	 *            is the string of participants
	 */
	public EventEntity(int id, String userId, String topic, long startDate, long endDate, String participants) {
		super(id + "");
		this.userId = userId;
		this.topic = topic;
		this.participants = participants;
		this.startDate = getDate(startDate);
		this.endDate = getDate(endDate);
		this.start = new DateTime(this.startDate.getTime());
		this.end = new DateTime(this.endDate.getTime());
	}

	/**
	 * Gets a calendar out of a unix timestamp (in seconds)
	 * 
	 * @param timestamp
	 *            is the timestamp in seconds
	 * @return a calendar object out of the timestamp
	 */
	private java.util.Calendar getDate(long timestamp) {
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(timestamp * 1000);
		return date;

	}

	/**
	 * Gets the date out of a string for date and a string for time
	 * 
	 * @param date
	 *            is the date string
	 * @param time
	 *            is the time string
	 * @return a calendar object of the date and time
	 */
	private java.util.Calendar getDate(String date, String time) {
		java.util.Calendar startDate = new GregorianCalendar();
		String dateSplit[] = date.split("-");
		String timeSplit[] = time.split(":");
		int year = Integer.parseInt(dateSplit[0]);
		int month = Integer.parseInt(dateSplit[1]) - 1;
		int day = Integer.parseInt(dateSplit[2]);
		int hour = Integer.parseInt(timeSplit[0]);
		int minutes = Integer.parseInt(timeSplit[1]);
		startDate.set(java.util.Calendar.MONTH, month);
		startDate.set(java.util.Calendar.DAY_OF_MONTH, day);
		startDate.set(java.util.Calendar.YEAR, year);
		startDate.set(java.util.Calendar.HOUR_OF_DAY, hour);
		startDate.set(java.util.Calendar.MINUTE, minutes);
		startDate.set(java.util.Calendar.SECOND, 0);
		return startDate;
	}

	/**
	 * Creates an event and returns an ical file
	 * 
	 * @return an ical file of the event
	 */
	public File createEvent() {
		File f = this.iCalFile = createEvent(startDate, endDate);
		return f;
	}

	/**
	 * Updates the event and returns an ical file
	 * 
	 * @return an ical file of the event
	 */
	public File updateEvent() {
		File f = this.iCalFile = createEvent(startDate, endDate);
		return f;
	}

	/**
	 * Edits an event
	 * 
	 * @param topic
	 *            is the new topic
	 * @param date
	 *            is the new date
	 * @param time
	 *            is the new time
	 * @param participants
	 *            is the array of participants
	 * @return true if editing the event was successful, otherwise false
	 */
	public boolean editEvent(String topic, String date, String time, String... participants) {
		this.topic = topic;
		this.startDate = getDate(date, time);
		this.endDate = getDate(date, time);
		this.participants = getParticipantString(participants);
		return true;
	}

	/**
	 * Creates an event and outputs it to a temporary file in the mentioned
	 * output directory
	 * 
	 * @param date
	 *            is the date of the event
	 * @param time
	 *            is the time of the event
	 * @return is the ical file
	 */
	private File createEvent(Calendar startDate, Calendar endDate) {
		this.start = new DateTime(startDate.getTime());
		this.end = new DateTime(endDate.getTime());
		VEvent event = new VEvent(start, end, topic);
		Description d = new Description();
		d.setValue(participants);
		event.getProperties().add(d);
		net.fortuna.ical4j.model.Calendar icsCalendar = createCalendar();
		icsCalendar.getComponents().add(event);
		File f = null;
		try {
			UidGenerator ug = new UidGenerator("uidGen");
			Uid uid = ug.generateUid();
			event.getProperties().add(uid);
			Random r = new Random();
			String file = outputPath + File.separator + r.nextInt(1000) + ".ics";
			FileOutputStream fout = new FileOutputStream(file);
			CalendarOutputter outputter = new CalendarOutputter();
			outputter.output(icsCalendar, fout);
			f = new File(file);
		} catch (ValidationException | IOException e) {
			System.err.println(e.getMessage());
		}
		return f;
	}

	/**
	 * Gets a string out of the array of strings for the participants
	 * 
	 * @param participants
	 *            is the array of stirngs
	 * @return participants as a string
	 */
	private String getParticipantString(String[] participants) {
		StringBuilder sb = new StringBuilder();
		sb.append("Participants:");
		for (String s : participants) {
			sb.append(" " + s);
		}
		return sb.toString();
	}

	/**
	 * Creates a ical4j calendar object
	 * 
	 * @return a ical4j calendar object
	 */
	private net.fortuna.ical4j.model.Calendar createCalendar() {
		net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
		icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
		icsCalendar.getProperties().add(Version.VERSION_2_0);
		icsCalendar.getProperties().add(CalScale.GREGORIAN);
		return icsCalendar;
	}

	/**
	 * Gets the topic
	 * 
	 * @return the topic as String
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * Gets the start
	 * 
	 * @return the start as DateTime
	 */
	public DateTime getStart() {
		return start;
	}

	/**
	 * Gets the end
	 * 
	 * @return the end as DateTime
	 */
	public DateTime getEnd() {
		return end;
	}

	/**
	 * Gets the iCalFile
	 * 
	 * @return the iCalFile as File
	 */
	public File getiCalFile() {
		return iCalFile;
	}

	/**
	 * Gets the userId
	 * 
	 * @return the userId as String
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Gets the participants
	 * @return the participants as String
	 */
	public String getParticipants() {
		return participants;
	}

	/**
	 * Returns a string representation of this event
	 * 
	 * @return the event as string
	 */
	public String toString() {
		String startDate = start.toInstant().toString().substring(0, start.toInstant().toString().length() - 1);
		String endDate = end.toInstant().toString().substring(0, end.toInstant().toString().length() - 1);
		return "| " + id + " | " + topic + " | " + startDate + " | " + endDate + " | " + participants + " | ";
	}
}
