/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.entities;

import de.hadesrofl.jimmey.enums.Permission;

/**
 * 
 * <p>
 * <strong>last update:</strong> 25.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class represents a channel in the db
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class ChannelEntity extends BaseEntity {
	/**
	 * Name of the channel
	 */
	private String name;
	/**
	 * Id of the guild
	 */
	private GuildEntity guild;
	/**
	 * Permission of the bot for this channel
	 */
	private Permission permission;

	/**
	 * Constructor
	 * 
	 * @param id
	 *            is the of the channel
	 * @param name
	 *            is the name of the channel
	 * @param guild
	 *            is the guild of this channel
	 * @param permission
	 *            is the permission level for the bot
	 */
	public ChannelEntity(String id, String name, GuildEntity guild, Permission permission) {
		super(id);
		this.name = name;
		this.guild = guild;
		this.permission = permission;
	}

	/**
	 * Gets the name
	 * 
	 * @return the name as String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the guild entity
	 * 
	 * @return the guild as guild entity
	 */
	public GuildEntity getGuildId() {
		return guild;
	}

	/**
	 * Gets the permission
	 * 
	 * @return the permission as Permission
	 */
	public Permission getPermission() {
		return permission;
	}

	/**
	 * Returns a string representation of this channel entity
	 * 
	 * @return this channel entity as a string
	 */
	public String toString() {
		return "| " + id + " | " + name + " | " + permission + " |";
	}
}
