/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.entities;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class represents Todos as used by the
 * {@link de.hadesrofl.jimmey.commands.TodoCommand TodoCommand}.
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class TodoEntity extends BaseEntity {
	/**
	 * The name of the todo
	 */
	private String topic;
	/**
	 * Is the id of the user
	 */
	private String userId;

	/**
	 * Constructor
	 * 
	 * @param topic
	 *            is the name of the todo
	 * @param userId
	 *            is the id of the user
	 */
	public TodoEntity(String topic, String userId) {
		super(0 + "");
		this.topic = topic;
		this.userId = userId;
	}

	/**
	 * Constructor
	 * 
	 * @param id
	 *            is the id of this todo
	 * @param topic
	 *            is the topic of this todo
	 * @param userId
	 *            is the id of the user
	 */
	public TodoEntity(int id, String topic, String userId) {
		super(id + "");
		this.topic = topic;
		this.userId = userId;
	}

	/**
	 * Gets the topic
	 * 
	 * @return the topic as String
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * Sets the new topic
	 * 
	 * @param topic
	 *            is the topic to set
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}

	/**
	 * Gets the userId
	 * 
	 * @return the userId as String
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Compares to todo objects if they are equal
	 * 
	 * @param t
	 *            is the todo object to compare against this object
	 * @return true if they are equal, otherwise false
	 */
	public boolean equals(TodoEntity t) {
		if (this.getId() == t.getId()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns a string representing this todo
	 * 
	 * @return this todo as a string
	 */
	public String toString() {
		return "| " + id + " |\t | **" + topic + "**|";
	}
}
