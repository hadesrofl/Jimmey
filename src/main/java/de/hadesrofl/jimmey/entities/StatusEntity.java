/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.entities;

import java.util.concurrent.TimeUnit;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class a status in the database
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class StatusEntity extends BaseEntity {
	/**
	 * name of the status
	 */
	private String name;
	/**
	 * time the user stayed in the status
	 */
	private long time;
	/**
	 * id of the user
	 */
	private String userId;

	/**
	 * Constructor
	 * 
	 * @param id
	 *            is the id of the status
	 * @param name
	 *            is the name of the status
	 * @param time
	 *            is the time a user stayed in this status
	 * @param userId
	 *            is the id of the user of this status
	 */
	public StatusEntity(String id, String name, long time, String userId) {
		super(id);
		this.name = name;
		this.time = time;
		this.userId = userId;
	}

	/**
	 * Gets the name
	 * 
	 * @return the name as String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the time
	 * 
	 * @return the time as long
	 */
	public long getTime() {
		return time;
	}

	/**
	 * Gets the userId
	 * 
	 * @return the userId as String
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Represents a status as a string
	 * 
	 * @return this status as a string
	 */
	public String toString() {
		String time = // hh:mm:ss
				String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(this.time),
						TimeUnit.MILLISECONDS.toMinutes(this.time)
								- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(this.time)),
						TimeUnit.MILLISECONDS.toSeconds(this.time)
								- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(this.time)));
		return "| " + id + " | " + name + " | " + time + " |";
	}

}
