/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package de.hadesrofl.jimmey;

import org.json.JSONObject;

import de.hadesrofl.jimmey.bot.BaseBot;
import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.utils.JsonReader;

/**
 * 
 * 
 * <p>
 * <strong>last update:</strong> 16.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * Launches this app and therefore the bot. A bot needs to be registered as app
 * at discord and then be added to your server via
 * <a href="https://discordapp.com/oauth2/authorize?client_id=243802274025963521&scope=bot&permissions=0x14028800">such a link</a>
 * where the permission is a bitmask and the client id given by discord
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.21
 */
public class BotLauncher {
	/**
	 * Singleton instance of the bot
	 */
	public static JimmeyBot INSTANCE;

	/**
	 * Creates a bot instances and logs it in
	 * 
	 * @param args
	 *            is not used
	 */
	public static void main(String[] args) {
		try {
			JSONObject botConfig = JsonReader.readFile("resources/config.json").getJSONObject("bot-config");
			String token = botConfig.getString("token");
			String prefix = botConfig.getString("prefix");
			String username = botConfig.getString("username");
			String timezone = botConfig.getString("timezone");
			String avatarFile = botConfig.getString("avatar");
			String iCalOutput = botConfig.getString("ICalOutput");
			long reminderDelay = botConfig.getJSONObject("reminder").getLong("delay");
			long reminderPeriod = botConfig.getJSONObject("reminder").getLong("period");
			long CET = botConfig.getLong("cleanerExecutionTime");
			INSTANCE = new JimmeyBot(BaseBot.login(token), prefix, username, timezone, reminderDelay, reminderPeriod,
					CET, avatarFile, iCalOutput);
		} catch (Exception e) {
			System.err.println("An error occured while loading the config.json...");
		}
	}

}
