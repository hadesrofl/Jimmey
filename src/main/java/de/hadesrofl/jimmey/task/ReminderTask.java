/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package de.hadesrofl.jimmey.task;

import java.time.LocalTime;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TimerTask;

import de.hadesrofl.jimmey.utils.Reminder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * 
 * 
 * <p>
 * <strong>last update:</strong> 13.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * A class to start a routine to check for reminders to trigger and message a
 * user who set the reminder
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.15
 */
public class ReminderTask extends TimerTask {
	/**
	 * Client to where the bot is connected to
	 */
	private IDiscordClient client;
	/**
	 * Queue of reminders to trigger for the users
	 */
	private Queue<Reminder> reminder;

	/**
	 * Constructor
	 * 
	 * @param client
	 *            is the IDiscordClient
	 */
	public ReminderTask(IDiscordClient client) {
		this.client = client;
		this.reminder = new PriorityQueue<Reminder>();
	}

	/**
	 * Runs the task and checks if any reminder is due to trigger
	 */
	public void run() {
		for (int i = 0; i < reminder.size(); i++) {
			Reminder r = reminder.peek();
			int currentTime = LocalTime.now().toSecondOfDay();
			if ((r.getTimeToFire() - currentTime) <= 0) {
				try {
					if (r.getUser() != null && r.getTopic() != null) {
						this.client.getOrCreatePMChannel(r.getUser()).sendMessage("Remind for " + r.getTopic());
					}
					reminder.poll();
				} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
					System.err.println(e.getMessage());
				}
			} else {
				break;
			}
		}
	}

	/**
	 * Adds a new Reminder to the task routine
	 * 
	 * @param r
	 *            is the new reminder to add to the task routine
	 */
	public void addReminder(Reminder r) {
		this.reminder.add(r);
	}

}
