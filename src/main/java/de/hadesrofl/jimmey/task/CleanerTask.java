/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.task;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TimerTask;

import de.hadesrofl.jimmey.utils.TimeMessage;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * 
 * 
 * <p>
 * <strong>last update:</strong> 13.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * This class is a task to clean old messages sent. This is used for messages
 * sent by the bot or to the bot (command calls)
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.15
 */
public class CleanerTask extends TimerTask {
	/**
	 * Queue of sent messages, sorted by the time it was sent
	 */
	private Queue<TimeMessage> sentMessages;
	/**
	 * Time to live for a message that was sent by the bot
	 */
	public final Long TTL = (long) 10 * 1000;

	/**
	 * Constructor
	 */
	public CleanerTask() {
		sentMessages = new PriorityQueue<TimeMessage>();
	}

	@Override
	public void run() {
		if (!sentMessages.isEmpty()) {
			for (int i = 0; i < sentMessages.size(); i++) {
				TimeMessage m = sentMessages.peek();
				long livedTime = System.currentTimeMillis() - m.getTime();
				if (livedTime > TTL) {
					sentMessages.poll();
					try {
						if (!m.getMessage().getChannel().isPrivate()) {
							m.getMessage().delete();
						}
					} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
						System.err.println(e.getMessage());
					}
				} else {
					break;
				}
			}
		}
	}

	/**
	 * Adds a message to the cleaner
	 * 
	 * @param message
	 *            is the IMessage object to clean
	 */
	public void addMessage(IMessage message) {
		this.sentMessages.add(new TimeMessage(message, System.currentTimeMillis()));
	}

}
