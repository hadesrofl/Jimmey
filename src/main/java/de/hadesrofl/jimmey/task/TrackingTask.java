/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.task;

import java.util.HashMap;
import java.util.Map;

import de.hadesrofl.jimmey.commands.GameCommand;
import de.hadesrofl.jimmey.enums.CommandEnum;
import sx.blah.discord.handle.impl.events.StatusChangeEvent;
import sx.blah.discord.handle.obj.IUser;

/**
 * 
 * <p>
 * <strong>last update:</strong> 24.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * This class represents a task that handles all active statuses and after a
 * user leaves the status it gives the {@link GameCommand} the user, status
 * and time the user was active in that status
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.21
 */
public class TrackingTask {
	/**
	 * Map of active statuses from the user
	 */
	private Map<IUser, Map<String, Long>> activeStatusMap;
	/**
	 * Tracking command used to give the active time of the status to the
	 * command
	 */
	private GameCommand command;

	/**
	 * Constructor
	 */
	public TrackingTask() {
		this.activeStatusMap = new HashMap<IUser, Map<String, Long>>();
		boolean found = false;
		for (CommandEnum e : CommandEnum.values()) {
			if (!found) {
				if (e.getCommand() instanceof GameCommand) {
					this.command = (GameCommand) e.getCommand();
					found = true;
				}
			}
		}
	}

	/**
	 * Adds a status to the active status map
	 * 
	 * @param event
	 *            is the StatusChangeEvent
	 * @param time
	 *            is the begin time of the status
	 * @return true if adding the status was successful, otherwise false
	 */
	public boolean addStatus(StatusChangeEvent event, long time) {
		IUser user = event.getUser();
		if (!activeStatusMap.containsKey(user)) {
			activeStatusMap.put(user, new HashMap<String, Long>());
		}
		Map<String, Long> userMap = activeStatusMap.get(user);
		if (userMap.containsKey(event.getNewStatus())) {
			userMap.put(event.getNewStatus().getStatusMessage(),
					userMap.get(event.getNewStatus()) + time);
		} else {
			userMap.put(event.getNewStatus().getStatusMessage(), time);
		}
		return true;
	}

	/**
	 * Processes a status and calculates the time the user was in the status and
	 * writes it into the list of the tracking command. After that the status
	 * will be removed from the active status map
	 * 
	 * @param event
	 *            is the StatusChangeEvent
	 * @param time
	 *            is the end time of the status
	 * @return true if the processing was successful, otherwise false
	 */
	public boolean processStatus(StatusChangeEvent event, long time) {
		if (!activeStatusMap.containsKey(event.getUser())
				|| !activeStatusMap.get(event.getUser()).containsKey(
						event.getOldStatus().getStatusMessage())) {
			return false;
		}
		Map<String, Long> userMap = activeStatusMap.get(event.getUser());
		long diff = time - userMap.get(event.getOldStatus().getStatusMessage());
		command.addStatus(event.getUser(), event.getOldStatus().getStatusMessage(), diff);
		userMap.remove(event.getOldStatus().getStatusMessage());
		return true;
	}

}
