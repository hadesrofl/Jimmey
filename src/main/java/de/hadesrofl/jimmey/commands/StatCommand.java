/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.commands;

import java.time.LocalDateTime;

import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;
import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;

/**
 * 
 * 
 * <p>
 * <b>last update:</b> 27.11.2016
 * </p>
 * <b>Description:</b>
 * <p>
 * This class respresents the stat command to show the stats of the current
 * guild
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class StatCommand extends BaseCommand {
	/**
	 * Constructor
	 * 
	 * @param keyword
	 *            is the keyword for this command
	 * @param bot
	 *            is the instance of this bot
	 */
	public StatCommand(String keyword, JimmeyBot bot) {
		super(keyword, bot);
		this.helpMsg = "Please enter (~ is used as the default prefix in this message, write help as a mention to the bot to get informed "
				+ "what kind of prefix is used):\n"
				+ "~stats --> to show the stats of this guild\n";
		this.errorMsg = "Error while getting stats of this Guild. Sorry :-(";
	}

	/**
	 * Processes the stat command and returns a message with the stats for the
	 * guild
	 * 
	 * @param message
	 *            is the IMessage object to process
	 * @param filteredContent
	 *            is the content to process
	 * @return the {@link ProcessStateEnum process status}
	 */
	public ProcessStateEnum process(IMessage message, String filteredContent) {
		if(bot == null){
			updateBotReference();
		}
		ProcessStateEnum processState = ProcessStateEnum.NOT_PROCESSED;
		String split[] = filteredContent.split(" ");
		String send = "";
		try {
			if (split.length > 1 && split[1].startsWith("-help")) {
				send = "```\n" + helpMsg + "```";
				sendMessage(message, send, true);
				processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
			} else {
				send = "Stats of this Guild: \n\n";
				int noChannels = message.getGuild().getChannels().size();
				int noInvites = message.getGuild().getInvites().size();
				String owner = message.getGuild().getOwner().toString();
				LocalDateTime creationDateTime = message.getGuild()
						.getCreationDate();
				String year = creationDateTime.getYear() + "";
				String month = creationDateTime.getMonthValue() + "";
				String day = creationDateTime.getDayOfMonth() + "";
				String hour = creationDateTime.getHour() + "";
				String minutes = creationDateTime.getMinute() + "";
				String creationDate = year + "-" + month + "-" + day + " "
						+ hour + ":" + minutes;
				int noBanned = message.getGuild().getBannedUsers().size();
				String region = message.getGuild().getRegion().toString();
				String gname = message.getGuild().getName();
				int noRoles = message.getGuild().getRoles().size();
				int noUsers = message.getGuild().getUsers().size();
				int noVChannels = message.getGuild().getVoiceChannels().size();
				send += "**Guild:** " + gname + "\n";
				send += "**Owner:** " + owner + "\n";
				send += "**Region:** " + region + "\n";
				send += "**Creation Date:** " + creationDate + "\n";
				send += "**Number of Channels:** " + noChannels + "\n";
				send += "**Number of Voice Channels:** " + noVChannels + "\n";
				send += "**Number of Users:** " + noUsers + "\n";
				send += "**Number of banned users:** " + noBanned + "\n";
				send += "**Number of Invites:** " + noInvites + "\n";
				send += "**Number of Roles:** " + noRoles + "\n";
				sendMessage(message, send, false);
				processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
			}
		} catch (DiscordException | RateLimitException
				| MissingPermissionsException e) {
			sendMessage(message, errorMsg, true);
			processState = ProcessStateEnum.PROCESSED_W_ERROR;
		}
		return processState;
	}

}
