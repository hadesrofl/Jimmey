/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package de.hadesrofl.jimmey.commands;

import de.hadesrofl.jimmey.enums.CommandEnum;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import sx.blah.discord.handle.obj.IMessage;

/**
 * 
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * Checks for the commands supported by this bot and reacts accordingly
 * 
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class CommandChecker {
	/**
	 * Prefix for commands
	 */
	private String prefix;

	/**
	 * Constructor
	 * 
	 * @param prefix
	 *            is the prefix for commands
	 */
	public CommandChecker(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * Checks for given commands if the message contains any of the keywords for
	 * the commands
	 * 
	 * @param message
	 *            is the IMessage object received through an event
	 * @param mention
	 *            is the call based on a mention or a message without a mention
	 *            of the bot
	 * @return the received {@link ProcessStateEnum processing status}
	 */
	public ProcessStateEnum checkCommands(IMessage message, boolean mention) {
		String filteredContent = filterContent(message);
		for (CommandEnum c : CommandEnum.values()) {
			if (filteredContent.startsWith(c.getKeyword())) {
				return c.process(message, filteredContent);
			}
		}
		return ProcessStateEnum.NOT_PROCESSED;
	}

	/**
	 * Filters a messages content and frees it from mentions or the
	 * {@link de.hadesrofl.jimmey.bot.BaseBot#getPrefix() command prefix}
	 * 
	 * @param message
	 *            is the message containing the content
	 * @return is a filtered content free from mentions or command prefix
	 */
	private String filterContent(IMessage message) {
		String content = message.getContent();
		String filteredContent = "";
		String tmp[] = null;
		int index = -1;
		if (content.startsWith("@") || content.startsWith("<@") || content.startsWith(this.prefix)) {
			tmp = content.substring(1).split(" ");
			if (content.startsWith(this.prefix)) {
				index = 0;
			} else {
				index = 1;
			}
		}
		if (index >= 0 && tmp != null) {
			for (; index < tmp.length; index++)
				filteredContent += tmp[index] + " ";
		}
		return filteredContent;
	}
}
