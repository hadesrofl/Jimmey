/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.commands;

import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import sx.blah.discord.handle.obj.IMessage;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class represents the help command and sends a message to the user
 * containing tips for using this bot
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class HelpCommand extends BaseCommand {
	/**
	 * Constructor
	 * 
	 * @param keyword
	 *            is the word to call the command with
	 * @param bot
	 *            is the instance of the bot
	 */
	public HelpCommand(String keyword, JimmeyBot bot) {
		super(keyword, bot);
		this.errorMsg = "";
		this.helpMsg = "Following commands are supported (~ is used as the default prefix in this message, write help as a mention to the bot to get informed "
				+ "what kind of prefix is used):\n" + "~games --> shows a list of your games and your time in game"
				+ "~help --> prints this help\n"
				+ "~permission --> to set permissions for the bot for specific channels\n"
				+ "~remind --> to remind you about something\n" + "~schedule --> to schedule events\n"
				+ "~stats --> shows the stats of this guild" + "~time --> returns you the current time of Jimmey\n"
				+ "~todo --> manages your todo list"
				+ "all commands have the -help subcommand to check for further details e.g.~schedule -help";
	}

	/**
	 * Processes the help command
	 * 
	 * @param message
	 *            is the IMessage object to process
	 * @param filteredContent
	 *            is the content to process
	 * @return the {@link ProcessStateEnum process status}
	 */
	public ProcessStateEnum process(IMessage message, String filteredContent) {
		if (bot == null) {
			updateBotReference();
		}
		String send = "```\n";
		send += "The used prefix for the bot is " + bot.getPrefix() + "\n\n";
		send += helpMsg;
		send += "```";
		sendMessage(message, send, true);
		return ProcessStateEnum.SUCCESFULLY_PROCESSED;
	}
}
