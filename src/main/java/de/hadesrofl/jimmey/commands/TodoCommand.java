/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.commands;

import java.util.InputMismatchException;
import java.util.List;
import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.entities.TodoEntity;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import de.hadesrofl.jimmey.enums.UTC;
import de.hadesrofl.jimmey.utils.database.DBConnection;
import sx.blah.discord.handle.obj.IMessage;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class represents the Todo command and creates a list of todos for a user
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class TodoCommand extends BaseCommand {

	/**
	 * Constructor
	 * 
	 * @param keyword
	 *            is the keyword of this command
	 * @param bot
	 *            is the instance of the bot
	 */
	public TodoCommand(String keyword, JimmeyBot bot) {
		super(keyword, bot);
		this.helpMsg = "Please enter (~ is used as the default prefix in this message, write help as a mention to the bot to get informed "
				+ "what kind of prefix is used):\n" + "~todo --> to show todo list\n"
				+ "~todo -show --> to show todo list\n" + "~todo Stuff --> to add *Stuff* to your todo list\n"
				+ "~todo -edit *ID* *New Todo Name* --> to change the todo name of the todo with the given id\n"
				+ "~todo -remove *ID* --> to delete the todo with the given id\n";
		this.errorMsg = "Oh, something went wrong. Please check the syntax: \n" + helpMsg;
	}

	/**
	 * Processes the time command and returns a message containing the current
	 * time in a given {@link UTC} timezone
	 * 
	 * @param message
	 *            is the IMessage object to process
	 * @param filteredContent
	 *            is the content to process
	 * @return the {@link ProcessStateEnum process status}
	 */
	public ProcessStateEnum process(IMessage message, String filteredContent) {
		if(bot == null){
			updateBotReference();
		}
		ProcessStateEnum processState = ProcessStateEnum.NOT_PROCESSED;
		String split[] = filteredContent.split(" ");
		String send = "```" + "\n";
		try {
			if (split.length == 1 || split.length > 1 && split[1].compareTo("-show") == 0) {
				sendMessage(message, show(message, send), true);
				processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
			} else if (split.length > 1) {
				switch (split[1]) {
				case "-help":
					send += helpMsg;
					send += "```";
					sendMessage(message, send, true);
					processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
					break;
				case "-edit":
					if (split.length < 3) {
						throw new Exception("Edit command is too short!");
					} else {
						send = "ID: " + split[2] + " changed to ";
						for (int i = 3; i < split.length; i++) {
							send += split[i] + " ";
						}
						if (edit(message, split)) {
							reply(message, send);
							processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
						} else {
							throw new Exception("Can't edit, maybe a syntax problem or the todo is not found!");
						}
					}
					break;
				case "-remove":
					send = "deleted ID: " + split[2];
					if (remove(message, split)) {
						reply(message, send);
						processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
					} else {
						throw new Exception("Can't edit, maybe a syntax problem or the todo is not found!");
					}
					break;
				default:
					String topic = add(message, split);
					if (topic != null) {
						send = "added " + topic + " to Todo-List!";
						reply(message, send);
						processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
					} else {
						throw new Exception("Error while adding a new todo!");
					}
					break;
				}
			}
		} catch (Exception e) {
			send += errorMsg;
			send += "```";
			sendMessage(message, send, true);
			processState = ProcessStateEnum.PROCESSED_W_ERROR;
		}
		return processState;
	}

	/**
	 * Processes the sub command show
	 * 
	 * @param message
	 *            is the IMessage object for the todo command
	 * @param send
	 *            is the send string for the reply
	 * @return the message to send as string
	 */
	private String show(IMessage message, String send) {
		List<TodoEntity> todos = DBConnection.getInstance().getQm().findAllTodosByUser(message.getAuthor().getID());
		if (todos != null) {
			send = "Your Todo-List contains: \n| ID |\t | Name |\n";
			if (todos.size() == 0) {
				send = "Your Todo-List is empty!";
			} else {
				for (TodoEntity t : todos) {
					send += t + "\n";
				}
			}
		} else {
			send = "Your Todo-List is empty!";
		}
		return send;
	}

	/**
	 * Adds a todo to the todo list of an user
	 * 
	 * @param message
	 *            is the IMessage object which called the todo command
	 * @param split
	 *            is the content as string array
	 * @return the topic that was added to the todo list
	 */
	private String add(IMessage message, String[] split) {
		String topic = "";
		for (int i = 1; i < split.length; i++) {
			topic += split[i] + " ";
		}
		topic = topic.substring(0, topic.length() - 1);
		if (DBConnection.getInstance().getQm().insertTodo(topic, message.getAuthor().getID()) < 0) {
			topic = null;
		}
		return topic;
	}

	/**
	 * Edits a todo entry
	 * 
	 * @param message
	 *            is the IMessage object that called the todo command
	 * @param split
	 *            is the content as string array
	 * @return true if the edit was successful, otherwise false if the todo
	 *         could not be found
	 */
	private boolean edit(IMessage message, String[] split) {
		try {
			int id = Integer.parseInt(split[2]);
			TodoEntity t = getTodo(message, id);
			if (t == null)
				return false;
			String newTopic = "";
			for (int i = 3; i < split.length; i++) {
				newTopic += split[i] + " ";
			}
			if (DBConnection.getInstance().getQm().updateTodo(Integer.parseInt(t.getId()), newTopic,
					t.getUserId()) > -1) {
				return true;
			} else {
				return false;
			}
		} catch (InputMismatchException e) {
			System.out.println("Can't convert the third string from the message as integer!");
			return false;
		}
	}

	/**
	 * Removes a todo from the todo list of an user
	 * 
	 * @param message
	 *            is the IMessage object that called the todo command
	 * @param split
	 *            is the content as string array
	 * @return true if the todo element could be deleted, otherwise false if the
	 *         element could not be found
	 */
	private boolean remove(IMessage message, String[] split) {
		try {
			int id = Integer.parseInt(split[2]);
			TodoEntity t = getTodo(message, id);
			if (t == null)
				return false;
			if (DBConnection.getInstance().getQm().deleteTodo(id) > -1) {
				return true;
			} else {
				return false;
			}
		} catch (InputMismatchException e) {
			System.out.println("Can't convert the third string from the message as integer!");
			return false;
		}
	}

	/**
	 * Gets a todo from the todo list of an user
	 * 
	 * @param message
	 *            is the IMessage object that called the todo command
	 * @param id
	 *            is the id of the todo
	 * @return the todo or null if the todo could not be found
	 */
	private TodoEntity getTodo(IMessage message, int id) {
		return DBConnection.getInstance().getQm().findTodo(id, message.getAuthor().getID());
	}
}
