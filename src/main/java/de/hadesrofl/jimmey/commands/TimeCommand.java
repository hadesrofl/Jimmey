/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.commands;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import de.hadesrofl.jimmey.BotLauncher;
import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import de.hadesrofl.jimmey.enums.UTC;
import de.hadesrofl.jimmey.utils.UTCChecker;
import sx.blah.discord.handle.obj.IMessage;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class represents the time command and tells the user the current time
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class TimeCommand extends BaseCommand {
	/**
	 * Constructor
	 * 
	 * @param keyword
	 *            is the keyword of this command
	 * @param bot
	 *            is the instance of the bot
	 */
	public TimeCommand(String keyword, JimmeyBot bot) {
		super(keyword, bot);
		this.errorMsg = "Something went wrong :-(.\nMaybe a wrong UTC format was entered? Check the help of this command for more specifics";
		this.helpMsg = "Please enter (~ is used as the default prefix in this message, write help as a mention to the bot to get informed "
				+ "what kind of prefix is used):\n" + "~time +01:00\n"
				+ "(where +01:00 is your UTC time zone) to get the current time of jimmey\n"
				+ "(for default time zone of Jimmey only writing " + "~time is fine)\nor\n" + "~time -help +01:00\n"
				+ "to get a list of cities/countries affected by the UTC time zone";
	}

	/**
	 * Processes the time command and returns a message containing the current
	 * time in a given {@link UTC} timezone
	 * 
	 * @param message
	 *            is the IMessage object to process
	 * @param filteredContent
	 *            is the content to process
	 * @return the {@link ProcessStateEnum process status}
	 */
	public ProcessStateEnum process(IMessage message, String filteredContent) {
		if(bot == null){
			bot = BotLauncher.INSTANCE;
		}
		String split[] = filteredContent.split(" ");
		String send = "";
		ProcessStateEnum processState = ProcessStateEnum.NOT_PROCESSED;
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
		// Help command
		if (split.length > 1 && split[1].compareTo("-help") == 0) {
			send = "```\n";
			if (split.length > 2 && UTCChecker.checkUTC(split[2])) {
				for (UTC u : UTC.values()) {
					if (u.getFormat().compareTo(split[2]) == 0) {
						send += u.toString() + "\n";
						break;
					}
				}
			} else {
				send += helpMsg;
				send += "Default Timezone: " + bot.getTimeZone() + "\n";
			}
			send += "```";
			sendMessage(message, send, true);
			processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
			// Given UTC
		} else if (split.length > 1) {
			try {
				String timezone = split[1];
				if (!UTCChecker.checkUTC(timezone))
					throw new Exception("The UTC format seems to be wrong!");
				send = "My current time is: " + ZonedDateTime.now(ZoneOffset.of(timezone)).format(dateFormatter)
						+ " UTC" + timezone;
				reply(message, send);
				processState = ProcessStateEnum.PROCESSED_W_ERROR;
			} catch (Exception e) {
				send = "```\n";
				send += errorMsg;
				send += "```";
				sendMessage(message, send, true);
				processState = ProcessStateEnum.PROCESSED_W_ERROR;
			}
			// no UTC --> default time zone as stated in config.json
		} else {
			send = "My current time is: " + ZonedDateTime.now(ZoneOffset.of(bot.getTimeZone())).format(dateFormatter)
					+ " UTC " + bot.getTimeZone();
			reply(message, send);
			processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
		}
		return processState;
	}
}
