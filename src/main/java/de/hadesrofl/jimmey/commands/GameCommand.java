/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.commands;

import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import java.util.List;

import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.entities.StatusEntity;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import de.hadesrofl.jimmey.utils.database.DBConnection;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * This class represents the game command and persists the active time in a
 * status per user
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class GameCommand extends BaseCommand {

	/**
	 * Constructor
	 * 
	 * @param keyword
	 *            is the keyword of this command
	 * @param bot
	 *            is the instance of the bot
	 */
	public GameCommand(String keyword, JimmeyBot bot) {
		super(keyword, bot);
		this.helpMsg = "Please enter (~ is used as the default prefix in this message, write help as a mention to the bot to get informed "
				+ "what kind of prefix is used):\n" + "~tracking --> to show game list\n"
				+ "~tracking -show --> to show game list\n";
		this.errorMsg = "```\nError in Tracking command!\n\n" + helpMsg + "```";
	}

	/**
	 * Processes the game command
	 * 
	 * @param message
	 *            is the IMessage object to process
	 * @param filteredContent
	 *            is the content to process
	 * @return the {@link ProcessStateEnum process status}
	 */
	public ProcessStateEnum process(IMessage message, String filteredContent) {
		if(bot == null){
			updateBotReference();
		}
		ProcessStateEnum processState = ProcessStateEnum.NOT_PROCESSED;
		try {
			String send = "";
			String split[] = filteredContent.split(" ");
			if (split.length > 1) {
				String subCommand = split[1];
				switch (subCommand) {
				case "-help":
					send = "```" + "\n";
					send += helpMsg;
					send += "```";
					sendMessage(message, send, true);
					processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
					break;
				case "-show":
					sendMessage(message, show(message), false);
					processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
					break;
				default:
					sendMessage(message, errorMsg, true);
					processState = ProcessStateEnum.PROCESSED_W_ERROR;
					break;
				}
			} else {
				sendMessage(message, show(message), false);
				processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			processState = ProcessStateEnum.PROCESSED_W_ERROR;
		}
		return processState;
	}

	/**
	 * Shows the game list for a user
	 * 
	 * @param message
	 *            is the IMessage object
	 * @return a string containing the list of games and their status
	 */
	private String show(IMessage message) {
		List<StatusEntity> status = DBConnection.getInstance().getQm().findAllStatusByUser(message.getAuthor().getID());
		String send = "";
		if (status != null) {
			if (status.size() == 0) {
				send = "Your Game-List is empty!";
			} else {
				send = "Your Game-List consists of:\n\n| **ID** | **Name** | **HH:MM:SS** |\n";
				for (StatusEntity s : status) {
					send += s + "\n";
				}
			}
		} else {
			send = "Your Game-List is empty!";
		}
		return send;
	}

	/**
	 * Adds a game status to the database
	 * 
	 * @param user
	 *            is the user who was active on a status
	 * @param name
	 *            is the name of the status the user was active on
	 * @param time
	 *            is the active time on the status
	 * @return true if adding the time to the status was successful, otherwise
	 *         false
	 */
	public boolean addStatus(IUser user, String name, long time) {
		List<StatusEntity> status = DBConnection.getInstance().getQm().findAllStatusByUser(user.getID());
		StatusEntity foundStatus = null;
		boolean success = false;
		for (StatusEntity e : status) {
			if (e.getName().compareTo(name) == 0) {
				foundStatus = e;
				break;
			}
		}
		if (foundStatus == null) {
			if (DBConnection.getInstance().getQm().insertStatus(name, time, user.getID()) > 0) {
				success = true;
			} else {
				success = false;
			}
		} else {
			if (DBConnection.getInstance().getQm().updateStatus(Integer.parseInt(foundStatus.getId()), name,
					foundStatus.getTime() + time, user.getID()) > 0) {
				success = true;
			} else {
				success = false;
			}
		}
		return success;
	}

}
