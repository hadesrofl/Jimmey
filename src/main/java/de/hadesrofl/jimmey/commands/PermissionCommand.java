/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.commands;

import java.util.List;

import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.entities.ChannelEntity;
import de.hadesrofl.jimmey.enums.Permission;
import de.hadesrofl.jimmey.enums.PermissionMap;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import de.hadesrofl.jimmey.utils.database.DBConnection;
import sx.blah.discord.handle.obj.IMessage;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class represents the Permission command to set the permission level for
 * specific channels for the bot
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class PermissionCommand extends BaseCommand {
	/**
	 * Constructor
	 * 
	 * @param keyword
	 *            is the keyword for this command
	 * @param bot
	 *            is the instance of the bot
	 */
	public PermissionCommand(String keyword, JimmeyBot bot) {
		super(keyword, bot);
		this.helpMsg = "Please enter (~ is used as the default prefix in this message, write help as a mention to the bot to get informed "
				+ "what kind of prefix is used):\n" + "~permission --> to show todo list\n"
				+ "~permission -show --> to show todo list\n"
				+ "~permission -levels --> show different permission levels\n"
				+ "~permission -set **LEVEL** --> set permission level to the specific *Level* for the current channel\n"
				+ "~permission -set **ID** **LEVEL** --> set permission level to *listen* for channel with **ID**\n";
		this.errorMsg = "Oh, something went wrong. Please check the syntax: \n" + helpMsg;
	}

	/**
	 * Processes the permission command
	 * 
	 * @param message
	 *            is the IMessage object to process
	 * @param filteredContent
	 *            is the content to process
	 * @return the {@link ProcessStateEnum process status}
	 */
	public ProcessStateEnum process(IMessage message, String filteredContent) {
		if (bot == null) {
			updateBotReference();
		}
		String split[] = filteredContent.split(" ");
		ProcessStateEnum processState = ProcessStateEnum.NOT_PROCESSED;
		boolean success = false;
		try {
			if (split.length > 1) {
				String subCommand = split[1];
				switch (subCommand) {
				case "-help":
					sendMessage(message, helpMsg, true);
					success = true;
					break;
				case "-levels":
					sendMessage(message, showLevels(message), true);
					success = true;
					break;
				case "-set":
					Permission p = null;
					if (split.length > 3) {
						p = parsePermission(split[3]);
						if (p != null) {
							if (setPermission(split[2], message.getGuild().getID(),
									PermissionMap.getPermissionValue(p))) {
								success = true;
							}
						}
					} else if (split.length > 2) {
						p = parsePermission(split[2]);
						if (p != null) {
							if (setPermission(message, PermissionMap.getPermissionValue(p))) {
								success = true;
							}
						}
					}
					break;
				case "-show":
					sendMessage(message, show(message), false);
					success = true;
					break;
				default:
					sendMessage(message, show(message), false);
					success = true;
					break;
				}
			} else {
				sendMessage(message, show(message), false);
				success = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			processState = ProcessStateEnum.PROCESSED_W_ERROR;
		}
		if (success) {
			processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
		} else {
			processState = ProcessStateEnum.PROCESSED_W_ERROR;
		}
		return processState;
	}

	/**
	 * Parses a string into a permission
	 * 
	 * @param permission
	 *            is the permission given via the content of the message
	 * @return a permission object or null if none permission could be matched
	 */
	private Permission parsePermission(String permission) {
		for (Permission p : Permission.values()) {
			if (p.toString().compareTo(permission) == 0) {
				return p;
			}
		}
		return null;
	}

	/**
	 * Sets the level of permission for the current channel
	 * 
	 * @param message
	 *            is the IMessage object calling this command
	 * @param permission
	 *            is the level of permission as mentioned in the message
	 * @return true if the new permission level could be set, otherwise false
	 */
	private boolean setPermission(IMessage message, int permission) {
		if (DBConnection.getInstance().getQm().updateChannel(message.getChannel().getID(),
				message.getChannel().getName(), message.getGuild().getID(), permission) > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Sets the level of permission for a specific channel
	 * 
	 * @param channelId
	 *            is the id of the channel to set a new permission level for
	 * @param guildId
	 *            is the id of the guild of the channel
	 * @param permission
	 *            is the level of permission as mentioned in the message
	 * @return true if the new permission level could be set, otherwise false
	 */
	private boolean setPermission(String channelId, String guildId, int permission) {
		ChannelEntity c = DBConnection.getInstance().getQm().findChannel(channelId, guildId);
		if (c != null) {
			if (DBConnection.getInstance().getQm().updateChannel(channelId, c.getName(), guildId, permission) > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Returns a string of all available permission levels
	 * 
	 * @param message
	 *            is the IMessage object calling this command
	 * @return a string of all permission levels
	 */
	private String showLevels(IMessage message) {
		String send = "The following permission level exist:\n\n";
		for (Permission p : Permission.values()) {
			send += p + " < ";
		}
		send = send.substring(0, send.length() - 2);
		return send;
	}

	/**
	 * Returns a string showing all channels and their permission levels
	 * 
	 * @param message
	 *            is the IMessage object calling this command
	 * @return a string containing all channels and their permission levels
	 */
	private String show(IMessage message) {
		String send = "";
		List<ChannelEntity> channels = DBConnection.getInstance().getQm()
				.findAllChannelsByGuild(message.getGuild().getID());
		if (channels != null) {
			send = "This guild has the following channels:\n\n|**ID**|**Name**|**Permission**|\n";
			for (ChannelEntity c : channels) {
				send += c + "\n";
			}
		} else {
			send = "No channels listed for this guild!";
		}
		return send;
	}

}
