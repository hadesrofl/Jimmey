/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.entities.EventEntity;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import de.hadesrofl.jimmey.utils.database.DBConnection;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class represents the schedule command and after successfully processing
 * the command it sends a ical file to the user containing the scheduled event
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class ScheduleCommand extends BaseCommand {
	/**
	 * Constructor
	 * 
	 * @param keyword
	 *            is the keyword of this command
	 * @param bot
	 *            is the instance of the bot
	 */
	public ScheduleCommand(String keyword, JimmeyBot bot) {
		super(keyword, bot);
		this.helpMsg = "Please enter an event in the following way (~ is used as the default prefix in this message, write help as a mention to the bot to get informed "
				+ "what kind of prefix is used):\n" + "~schedule - to show your scheduled events\n"
				+ "~schedule -show --> to show your scheduled events\n"
				+ "~schedule -edit *ID* *TOPIC* *DATE* *Participant1* *ParticipantN* --> to edit an existing event\n"
				+ "~schedule -remove *ID* --> to remove an event\n"
				+ "~schedule ExampleEvent 2016-12-07T18:00:00 Participant1 Participant2 ParticipantN";
		this.errorMsg = "Error in schedule command:\n\n" + helpMsg;
	}

	/**
	 * Processes the schedule command and sends an ical file to the user
	 * containing the event
	 * 
	 * @param message
	 *            is the IMessage object to process
	 * @param filteredContent
	 *            is the content to process
	 * @return the {@link ProcessStateEnum process status}
	 */
	public ProcessStateEnum process(IMessage message, String filteredContent) {
		if(bot == null){
			updateBotReference();
		}
		String split[] = filteredContent.split(" ");
		ProcessStateEnum processState = ProcessStateEnum.NOT_PROCESSED;
		if (split.length > 1) {
			try {
				String se = split[1];
				if (se.compareTo("-help") != 0) {
					switch (se) {
					case "-show":
						sendMessage(message, show(message), true);
						processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
						break;
					case "-edit":
						if (edit(message, split)) {
							processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
						} else {
							throw new Exception("Syntax error in schedule command!");
						}
						break;
					case "-remove":
						if (remove(message, split)) {
							processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
						} else {
							throw new Exception("Syntax error in schedule command!");
						}
						break;
					default:
						String sd = split[2];
						String date = sd.split("T")[0];
						String time = sd.split("T")[1];
						String participants[] = getParticipants(message, split, 3);
						EventEntity event = new EventEntity(message.getAuthor().getID(), se, date, time, participants);
						File iCalFile = event.createEvent();
						if (iCalFile != null) {
							message.getAuthor().getOrCreatePMChannel().sendFile(iCalFile);
							iCalFile.delete();
							insertEvent(event);
							processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
						} else {
							message.getAuthor().getOrCreatePMChannel()
									.sendMessage("Something went wrong while exporting this event as iCalFile");
							processState = ProcessStateEnum.PROCESSED_W_ERROR;
						}
						break;
					}
				} else {
					scheduleSyntaxError(message);
					return ProcessStateEnum.SUCCESFULLY_PROCESSED;
				}
			} catch (Exception e) {
				scheduleSyntaxError(message);
				processState = ProcessStateEnum.PROCESSED_W_ERROR;
			}
		} else {
			sendMessage(message, show(message), true);
			processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
		}
		return processState;
	}

	/**
	 * Edits an event
	 * 
	 * @param message
	 *            is the IMessage object that called the todo command
	 * @param split
	 *            is the content as string array
	 * @return true if the edit was successful, otherwise false if the event
	 *         could not be found
	 */
	private boolean edit(IMessage message, String[] split) {
		try {
			String id = split[2];
			EventEntity e = getEvent(id, message.getAuthor().getID());
			if (e == null)
				return false;
			String topic = split[3];
			String date = split[4].split("T")[0];
			String time = split[4].split("T")[1];
			String participants[] = getParticipants(message, split, 5);
			e.editEvent(topic, date, time, participants);
			File f = e.updateEvent();
			updateEvent(e);
			message.getAuthor().getOrCreatePMChannel().sendFile(f);
			f.delete();
			reply(message, " event updated!");
			return true;
		} catch (InputMismatchException | IOException | MissingPermissionsException | RateLimitException
				| DiscordException e) {
			System.out.println("Can't convert the third string from the message as integer!");
			return false;
		}
	}

	/**
	 * Shows the scheduled events of an user
	 * 
	 * @param message
	 *            is the IMessage object
	 * @return the events as a string
	 */
	private String show(IMessage message) {
		List<EventEntity> events = DBConnection.getInstance().getQm().findAllEventsByUser(message.getAuthor().getID());
		StringBuilder sb = new StringBuilder();
		sb.append("You have the following events scheduled:\n");
		sb.append("| **id** | **topic** | **startDate** | **endDate** | **participants** |\n");
		for (EventEntity e : events) {
			sb.append(e.toString() + "\n");
		}
		return sb.toString();
	}

	/**
	 * Removes an event from the list of events and database
	 * 
	 * @param message
	 *            is the IMessage object
	 * @param split
	 *            is the content of the message
	 * @return true if everything went fine, otherwise false
	 */
	private boolean remove(IMessage message, String[] split) {
		try {
			String id = split[2];
			if (DBConnection.getInstance().getQm().deleteEvent(id) > -1) {
				reply(message, " event deleted!");
				return true;
			} else {
				return false;
			}
		} catch (InputMismatchException e) {
			System.out.println("Can't convert the third string from the message as integer!");
			return false;
		}
	}

	/**
	 * Gets an event from the database
	 * 
	 * @param eventId
	 *            is the id of the event
	 * @param userId
	 *            is the user id
	 * @return the {@link EventEntity} from the database
	 */
	private EventEntity getEvent(String eventId, String userId) {
		return DBConnection.getInstance().getQm().findEvent(eventId, userId);
	}

	/**
	 * Gets the participants of a message and returns them in an array
	 * 
	 * @param message
	 *            is the IMessage object
	 * @param split
	 *            is the content of the message
	 * @param startIndex
	 *            is the starting index where the participants should start
	 * @return an array of the participants
	 */
	public String[] getParticipants(IMessage message, String[] split, int startIndex) {
		List<String> participants = new ArrayList<String>();
		for (int i = startIndex; i < split.length; i++) {
			// ignore mentions
			if (!split[i].startsWith("<@")) {
				participants.add(split[i]);
			}
		}
		// add mentions but not the bot
		for (IUser u : message.getMentions()) {
			if (bot.getClient().getOurUser().getID().compareTo(u.getID()) != 0)
				participants.add(u.getName());
		}
		String[] attendee = new String[participants.size()];
		for (int i = 0; i < participants.size(); i++) {
			attendee[i] = participants.get(i);
		}
		return attendee;
	}

	/**
	 * Sends a syntax error for the schedule command to let the user know what's
	 * the right way to invoke this command
	 * 
	 * @param message
	 *            is the IMessage object received through an event
	 */
	private void scheduleSyntaxError(IMessage message) {
		String send = "```";
		send += errorMsg;
		send += "```";
		sendMessage(message, send, true);
	}

	/**
	 * Inserts an event to the database
	 * 
	 * @param e
	 *            is the event to persist
	 * @return true on success, otherwise false
	 */
	private boolean insertEvent(EventEntity e) {
		if (DBConnection.getInstance().getQm().insertEvent(e.getStart().toInstant().getEpochSecond(),
				e.getEnd().toInstant().getEpochSecond(), e.getTopic(), e.getParticipants().replace("Participants:", ""),
				e.getUserId()) > -1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Updates an event in the database
	 * 
	 * @param e
	 *            is the event to update
	 * @return true on success, otherwise false
	 */
	private boolean updateEvent(EventEntity e) {
		if (DBConnection.getInstance().getQm().updateEvent(Integer.parseInt(e.getId()),
				e.getStart().toInstant().getEpochSecond(), e.getEnd().toInstant().getEpochSecond(), e.getTopic(),
				e.getParticipants().replace("Participants:", ""), e.getUserId()) > -1) {
			return true;
		} else {
			return false;
		}
	}

}
