/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.commands;

import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import de.hadesrofl.jimmey.utils.Reminder;
import de.hadesrofl.jimmey.utils.UTCChecker;
import sx.blah.discord.handle.obj.IMessage;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class represent the remind command and sents a reminder to the user via
 * {@link Reminder}.
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class RemindCommand extends BaseCommand {
	/**
	 * Constructor
	 * 
	 * @param keyword
	 *            is the keyword of this command
	 * @param bot
	 *            is the instance of the bot
	 */
	public RemindCommand(String keyword, JimmeyBot bot) {
		super(keyword, bot);
		this.helpMsg = "Please enter a reminder in the following way (~ is used as the default prefix in this message, write help as a mention to the bot to get informed "
				+ "what kind of prefix is used):\n"
				+ "~remind Topic 18:00:00 +01:00\nWhere +01:00 is your UTC time zone\n(for default time zone of Jimmey the UTC time zone can be dropped e.g.\n "
				+ "~remind Topic 10:50)";
		this.errorMsg = "Error in remind command:\n\n" + helpMsg;
	}

	/**
	 * Processes the remind command
	 * 
	 * @param message
	 *            is the IMessage object to process
	 * @param filteredContent
	 *            is the content to process
	 * @return the {@link ProcessStateEnum process status}
	 */
	public ProcessStateEnum process(IMessage message, String filteredContent) {
		if(bot == null){
			updateBotReference();
		}
		String split[] = filteredContent.split(" ");
		ProcessStateEnum processState = ProcessStateEnum.NOT_PROCESSED;
		if (split.length > 2) {
			try {
				String topic = split[1];
				if (topic.compareTo("-help") == 0) {
					remindSyntaxError(message);
					return ProcessStateEnum.SUCCESFULLY_PROCESSED;
				}
				String time = split[2];
				int timeToFire = 0;
				int remindTime;
				int current;
				// call with given UTC
				if (split.length > 3) {
					String timezone = split[3];
					if (!UTCChecker.checkUTC(timezone))
						throw new Exception("The UTC format seems to be wrong!");
					remindTime = LocalTime.parse(time).atOffset(ZoneOffset.of(timezone)).toLocalTime().toSecondOfDay();
					current = LocalTime.now().atOffset(ZoneOffset.of(timezone)).toLocalTime().toSecondOfDay();
				} else {
					remindTime = LocalTime.parse(time).atOffset(ZoneOffset.of(bot.getTimeZone())).toLocalTime()
							.toSecondOfDay();
					current = LocalTime.now().toSecondOfDay();
				}

				timeToFire = remindTime - current;
				if (timeToFire >= 0) {
					bot.getReminder().addReminder(new Reminder(message.getAuthor(), topic, remindTime));
					reply(message, " I'll remind you at " + time);
					processState = ProcessStateEnum.SUCCESFULLY_PROCESSED;
					// syntax error
				} else {
					DateTimeFormatter dateFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
					sendMessage(message,
							"That's in the past, right? I'll just remind you for future hours of this day :-)! Please enter a time > "
									+ LocalTime.now().format(dateFormatter) + " UTC " + bot.getTimeZone(),
							true);
					processState = ProcessStateEnum.PROCESSED_W_ERROR;
				}
			} catch (Exception e) {
				remindSyntaxError(message);
				processState = ProcessStateEnum.PROCESSED_W_ERROR;
			}
		} else {
			remindSyntaxError(message);
			processState = ProcessStateEnum.PROCESSED_W_ERROR;
		}
		return processState;
	}

	/**
	 * Sends a message to inform the user about the correct syntax for the
	 * remind command
	 * 
	 * @param message
	 *            is the IMessage object received through an event
	 * 
	 */
	private void remindSyntaxError(IMessage message) {
		String send = "```";
		send += errorMsg;
		send += "\n\nDefault time zone is: " + bot.getTimeZone() + "```";
		sendMessage(message, send, true);
	}

}
