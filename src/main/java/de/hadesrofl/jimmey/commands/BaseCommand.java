/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.commands;

import de.hadesrofl.jimmey.BotLauncher;
import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.enums.Permission;
import de.hadesrofl.jimmey.enums.ProcessStateEnum;
import de.hadesrofl.jimmey.utils.PermissionChecker;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * 
 * <p>
 * <strong>last update:</strong> 23.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * Base class for commands. Every command has a process method to process a
 * given message. Also every message needs to set help and error messages in
 * their constructor
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.21
 */
public abstract class BaseCommand {
	/**
	 * Keyword to call command
	 */
	protected String keyword = "";
	/**
	 * Error message for this command
	 */
	protected String errorMsg = "";
	/**
	 * Help message for this command
	 */
	protected String helpMsg = "";
	/**
	 * Instance of the bot
	 */
	protected JimmeyBot bot;

	/**
	 * Constructor
	 * 
	 * @param keyword
	 *            is the keyword for this command
	 * @param bot
	 *            is the bot instance
	 */
	public BaseCommand(String keyword, JimmeyBot bot) {
		this.bot = bot;
		this.keyword = keyword;
	}

	/**
	 * Gets the keyword
	 * 
	 * @return the keyword as String
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * Gets the errorMsg
	 * 
	 * @return the errorMsg as String
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * Gets the helpMsg
	 * 
	 * @return the helpMsg as String
	 */
	public String getHelpMsg() {
		return helpMsg;
	}

	/**
	 * Sends a message to the current channel as reply
	 * 
	 * @param message
	 *            is the IMessage object received through an event
	 * @param send
	 *            is the reply message
	 */
	protected void sendMessage(IMessage message, String send, boolean dm) {
		try {
			if (dm) {
				if (bot == null) {
					this.updateBotReference();
				}
				bot.getClient().getOrCreatePMChannel(message.getAuthor()).sendMessage(send);
			} else {
				if (PermissionChecker.checkPermission(message.getChannel().getID(), message.getGuild().getID(),
						Permission.WRITE)) {
					message.getChannel().sendMessage(send);
				} else {
					sendMessage(message,
							"Permission isn't high enough for me to send on this channel directly, so here's my reply:\n"
									+ send,
							true);
				}
			}
		} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Replies to a message with a text. Automatically mentions the user in the
	 * reply
	 * 
	 * @param message
	 *            is the original message
	 * @param reply
	 *            is the reply to the message
	 */
	protected void reply(IMessage message, String reply) {
		try {
			if (PermissionChecker.checkPermission(message.getChannel().getID(), message.getGuild().getID(),
					Permission.REPLY)) {
				message.reply(reply);
			} else {
				throw new Exception("Permission not high enough!");
			}
		} catch (MissingPermissionsException | RateLimitException | DiscordException e) {
			System.err.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			sendMessage(message,
					"Permission isn't high enough for me to send on this channel directly, so here's my reply:\n"
							+ reply,
					true);
		}
	}

	/**
	 * For testing purposes we need to set the bot instance of enums correctly
	 * without calling the main method
	 */
	public void updateBotReference() {
		this.bot = BotLauncher.INSTANCE;
	}

	/**
	 * Processes a command
	 * 
	 * @param message
	 *            is the IMessage object to process
	 * @param filteredContent
	 *            is the content to process
	 * @return an {@link ProcessStateEnum} informing about the state at the end
	 *         of the process
	 */
	abstract public ProcessStateEnum process(IMessage message, String filteredContent);
}
