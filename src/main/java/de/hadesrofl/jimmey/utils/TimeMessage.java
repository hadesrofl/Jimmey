/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package de.hadesrofl.jimmey.utils;

import sx.blah.discord.handle.obj.IMessage;

/**
 * 
 * 
 * <p>
 * <strong>last update:</strong> 13.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * A class to represent a tuple for a Message consisting of a IMessage object
 * and the time of e.g. creation or the sent time
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.15
 */
public class TimeMessage implements Comparable<TimeMessage> {
	/**
	 * Message object
	 */
	private IMessage message;
	/**
	 * time of message, used as sent time of message for delete in
	 * {@link de.hadesrofl.jimmey.task.CleanerTask CleanerTask}
	 */
	private long time;

	/**
	 * Constructor
	 * 
	 * @param message
	 *            is the IMessage object
	 * @param time
	 *            is the time for different purposes like sent time
	 */
	public TimeMessage(IMessage message, long time) {
		this.message = message;
		this.time = time;
	}

	/**
	 * Compares two TimeMessage objects.
	 * 
	 * @return 0 if the objects are equal, if this.time is greater than o.time it returns 1,
	 *         otherwise if this.time is less than o.time it returns -1
	 */
	public int compareTo(TimeMessage o) {
		if (this.time > o.getTime()) {
			return 1;
		} else if (this.time < o.getTime()) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * Gets the message
	 * 
	 * @return the message as IMessage
	 */
	public IMessage getMessage() {
		return message;
	}

	/**
	 * Gets the time
	 * 
	 * @return the time as long
	 */
	public long getTime() {
		return time;
	}

}
