/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.utils;

import de.hadesrofl.jimmey.entities.ChannelEntity;
import de.hadesrofl.jimmey.enums.Permission;
import de.hadesrofl.jimmey.enums.PermissionMap;
import de.hadesrofl.jimmey.utils.database.DBConnection;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class checks the permission for an operation
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class PermissionChecker {
	/**
	 * Checks if a permission for a channel is high enough for the according
	 * operation
	 * 
	 * @param channelId
	 *            is the id of the channel the operation will be executed on
	 * @param guildId
	 *            is the id of the guild of the channel
	 * @param minPerm
	 *            is the minimum of permission that is needed for the operation
	 * @return true if everything is fine, otherwise false if the permission
	 *         isn't high enough
	 */
	public static boolean checkPermission(String channelId, String guildId, Permission minPerm) {
		ChannelEntity c = DBConnection.getInstance().getQm().findChannel(channelId, guildId);
		if (c != null) {
			Permission channelPerm = c.getPermission();
			if (PermissionMap.getPermissionValue(channelPerm) >= PermissionMap.getPermissionValue(minPerm)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}
}
