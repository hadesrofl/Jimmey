/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.utils.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.json.JSONObject;

import de.hadesrofl.jimmey.utils.JsonReader;

/**
 * 
 * <p>
 * <strong>last update:</strong> 25.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class establishes a database connection to the sqlite db
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class DBConnection {
	/**
	 * Singleton of the DBConnection
	 */
	private static DBConnection INSTANCE;
	/**
	 * Database connection for sqlite
	 */
	private Connection connection;
	/**
	 * Manager of them all --> the queries
	 */
	private QueryManager qm;

	/**
	 * Constructor
	 * 
	 * @param db
	 *            is the path to the db
	 */
	private DBConnection(String db) {
		connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + db);
			qm = new QueryManager(connection);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Gets the instance of the DBConnection
	 * 
	 * @return the DBConnection object
	 */
	public static DBConnection getInstance() {
		if (INSTANCE == null) {
			JSONObject db = JsonReader.readFile("resources/config.json").getJSONObject("database");
			String dbName = db.getString("file");
			INSTANCE = new DBConnection(dbName);
		}
		return INSTANCE;
	}

	/**
	 * Gets the qm
	 * 
	 * @return the qm as QueryManager
	 */
	public QueryManager getQm() {
		return qm;
	}

	/**
	 * Closes the connection
	 */
	public void close() {
		try {
			if (connection != null) {
				connection.close();
			}

		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
