/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.utils.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import de.hadesrofl.jimmey.entities.ChannelEntity;
import de.hadesrofl.jimmey.entities.EventEntity;
import de.hadesrofl.jimmey.entities.GuildEntity;
import de.hadesrofl.jimmey.entities.StatusEntity;
import de.hadesrofl.jimmey.entities.TodoEntity;
import de.hadesrofl.jimmey.entities.UserEntity;
import de.hadesrofl.jimmey.enums.Permission;
import de.hadesrofl.jimmey.enums.PermissionMap;

/**
 * 
 * <p>
 * <strong>last update:</strong> 25.11.2016
 * </p>
 * <p>
 * <strong>Description:</strong>
 * </p>
 * <p>
 * This class manages the queries to the db
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class QueryManager {
	/**
	 * Connection to the db
	 */
	private Connection connection;

	/**
	 * Constructor
	 * 
	 * @param con
	 *            is the connection to the db
	 */
	public QueryManager(Connection con) {
		this.connection = con;
	}

	/**
	 * Finds a guild by a given id
	 * 
	 * @param guildId
	 *            is the id of the guild, that will be searched
	 * @return the Guild Entity or null if the guild wasn't found
	 */
	public GuildEntity findGuild(String guildId) {
		PreparedStatement pstmt = null;
		String query = "SELECT id, name FROM guilds where id = ?";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, guildId);
			ResultSet rs = pstmt.executeQuery();
			GuildEntity g = null;
			if (rs.next()) {
				g = new GuildEntity(rs.getString(1), rs.getString(2));
			}
			return g;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Finds all guilds in the db
	 * 
	 * @return a list of Guild Entities representing all guilds in the db or
	 *         null on error
	 */
	public List<GuildEntity> findAllGuilds() {
		String query = "SELECT * FROM guilds";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			List<GuildEntity> guilds = new LinkedList<GuildEntity>();
			while (rs.next()) {
				GuildEntity g = new GuildEntity(rs.getString(1), rs.getString(2));
				guilds.add(g);
			}
			return guilds;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Inserts a guild into the db
	 * 
	 * @param guildId
	 *            is the id of the guild
	 * @param guildName
	 *            is the name of the guild
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int insertGuild(String guildId, String guildName) {
		PreparedStatement pstmt = null;
		String query = "INSERT into guilds VALUES(?,?)";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, guildId);
			pstmt.setString(2, guildName);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Updates a guild in the db
	 * 
	 * @param guildId
	 *            is the id of the guild
	 * @param guildName
	 *            is the name of the guild
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int updateGuild(String guildId, String guildName) {
		PreparedStatement pstmt = null;
		String query = "UPDATE guilds SET name = ? WHERE id = ?";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, guildName);
			pstmt.setString(2, guildId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Deletes a guild from the db
	 * 
	 * @param guildId
	 *            is the id of the guild
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int deleteGuild(String guildId) {
		String query = "DELETE from guilds WHERE id = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, guildId);
			deleteGuildUsers(guildId, "0");
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Finds a channel in the db
	 * 
	 * @param channelId
	 *            is the id of the channel
	 * @param guildId
	 *            is the id of the guild of the channel
	 * @return the channel entity or null if the channel wasn't found
	 */
	public ChannelEntity findChannel(String channelId, String guildId) {
		String query = "SELECT id, name, guildid_ref, permission FROM channels where id = ? AND guildid_ref = ? ";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, channelId);
			pstmt.setString(2, guildId);
			ResultSet rs = pstmt.executeQuery();
			ChannelEntity c = null;
			if (rs.next()) {
				Permission p = PermissionMap.getPermission(rs.getInt(4));
				c = new ChannelEntity(rs.getString(1), rs.getString(2), findGuild(rs.getString(3)), p);
			}
			return c;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Finds all channels in the db
	 * 
	 * @return a list of channel entities or null on error
	 */
	public List<ChannelEntity> findAllChannels() {
		String query = "SELECT * FROM channels";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			List<ChannelEntity> channels = new LinkedList<ChannelEntity>();
			while (rs.next()) {
				Permission p = PermissionMap.getPermission(rs.getInt(4));
				ChannelEntity c = new ChannelEntity(rs.getString(1), rs.getString(2), findGuild(rs.getString(3)), p);
				channels.add(c);
			}
			return channels;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Finds all channels in the db for a specific guild
	 * 
	 * @param guildId
	 *            is the id of the guild
	 * @return a list of channel entities or null on error
	 */
	public List<ChannelEntity> findAllChannelsByGuild(String guildId) {
		String query = "SELECT * FROM channels WHERE guildid_ref = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, guildId);
			ResultSet rs = pstmt.executeQuery();
			List<ChannelEntity> channels = new LinkedList<ChannelEntity>();
			while (rs.next()) {
				Permission p = PermissionMap.getPermission(rs.getInt(4));
				ChannelEntity c = new ChannelEntity(rs.getString(1), rs.getString(2), findGuild(rs.getString(3)), p);
				channels.add(c);
			}
			return channels;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Inserts a channel into the db
	 * 
	 * @param channelId
	 *            is the id of the channel
	 * @param channelName
	 *            is the name of the channel
	 * @param guildId
	 *            is the id of the guild of the channel
	 * @param permission
	 *            is the permission level of the bot for this channel
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int insertChannel(String channelId, String channelName, String guildId, int permission) {
		PreparedStatement pstmt = null;
		String query = "INSERT into channels VALUES(?,?,?,?)";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, channelId);
			pstmt.setString(2, channelName);
			pstmt.setString(3, guildId);
			pstmt.setInt(4, permission);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Updates a channel in the db
	 * 
	 * @param channelId
	 *            is the id of the channel
	 * @param channelName
	 *            is the name of the channel
	 * @param guildId
	 *            is the id of the guild of the channel
	 * @param permission
	 *            is the permission level of the bot for the channel
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int updateChannel(String channelId, String channelName, String guildId, int permission) {
		PreparedStatement pstmt = null;
		String query = "UPDATE channels SET name = ? , permission = ? WHERE id = ? AND guildid_ref = ?";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, channelName);
			pstmt.setInt(2, permission);
			pstmt.setString(3, channelId);
			pstmt.setString(4, guildId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Deletes a channel from the db
	 * 
	 * @param channelId
	 *            is the id of the channel
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int deleteChannel(String channelId) {
		String query = "DELETE from channels WHERE id = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, channelId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Finds a user by a given id
	 * 
	 * @param userId
	 *            is the id of the user to search for
	 * @return the user entity or null if the user wasn't found
	 */
	public UserEntity findUser(String userId) {
		String query = "SELECT id, name, guildid_ref, userid_ref FROM users, guildusers WHERE id = ? AND userid_ref = ? ";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, userId);
			pstmt.setString(2, userId);
			ResultSet rs = pstmt.executeQuery();
			UserEntity u = null;
			if (rs.next()) {
				u = new UserEntity(rs.getString(1), rs.getString(2));
			}
			return u;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Finds all users in the db
	 * 
	 * @return a list of user entities or null on error
	 */
	public List<UserEntity> findAllUsers() {
		String query = "SELECT * FROM users";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			List<UserEntity> users = new LinkedList<UserEntity>();
			while (rs.next()) {
				UserEntity u = new UserEntity(rs.getString(1), rs.getString(2));
				users.add(u);
			}
			return users;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return new LinkedList<UserEntity>();
		}
	}

	/**
	 * Inserts a user into the db
	 * 
	 * @param userId
	 *            is the id of the user
	 * @param userName
	 *            is the name of the user
	 * @param guildId
	 *            is the id of the guild
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int insertUser(String userId, String userName, String guildId) {
		PreparedStatement pstmt = null;
		String query = "INSERT into users VALUES(?,?)";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, userId);
			pstmt.setString(2, userName);
			insertGuildUsers(userId, guildId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Updates a user in the db
	 * 
	 * @param userId
	 *            is the id of the user
	 * @param userName
	 *            is the name of the user
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int updateUser(String userId, String userName) {
		PreparedStatement pstmt = null;
		String query = "UPDATE users SET name = ? WHERE id = ?";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, userName);
			pstmt.setString(2, userId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Deletes a user from the db
	 * 
	 * @param userId
	 *            is the id of the user to delete from the db
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int deleteUser(String userId) {
		String query = "DELETE from users WHERE id = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, userId);
			deleteGuildUsers("0", userId);
			deleteStatus(userId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Inserts a guild with a responding user
	 * 
	 * @param userId
	 *            is the id of the user
	 * @param guildId
	 *            is the id of the guild
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	private int insertGuildUsers(String userId, String guildId) {
		PreparedStatement pstmt = null;
		String query = "INSERT into guildusers (guildid_ref, userid_ref) VALUES(?,?)";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, guildId);
			pstmt.setString(2, userId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Deletes a guild and user pair from the db table guildusers (m-n cross
	 * table for guilds and users)
	 * 
	 * @param guildId
	 *            is the id of the guild
	 * @param userId
	 *            is the id of the user
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	private int deleteGuildUsers(String guildId, String userId) {
		String query = "DELETE from guildusers WHERE guildid_ref = ? OR userid_ref = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, guildId);
			pstmt.setString(2, userId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Inserts an event into the database
	 * 
	 * @param startDate
	 *            is the start date of the event
	 * @param endDate
	 *            is the end date of the event
	 * @param topic
	 *            is the topic of the event
	 * @param participants
	 *            are the participants of the event as string
	 * @param userId
	 *            is the id of the user who creates this event
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int insertEvent(long startDate, long endDate, String topic, String participants, String userId) {
		PreparedStatement pstmt = null;
		String query = "INSERT into events (startDate, endDate, topic, participants, userid_ref)  VALUES(?,?,?,?,?)";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setLong(1, startDate);
			pstmt.setLong(2, endDate);
			pstmt.setString(3, topic);
			pstmt.setString(4, participants);
			pstmt.setString(5, userId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Updates an event in the database
	 * 
	 * @param eventId
	 *            is the id of the event
	 * @param startDate
	 *            is the new start date of the event
	 * @param endDate
	 *            is the new end date of the event
	 * @param topic
	 *            is the new topic of the event
	 * @param participants
	 *            are the new participants of the event
	 * @param userId
	 *            is the id of the user owning this event
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int updateEvent(int eventId, long startDate, long endDate, String topic, String participants,
			String userId) {
		PreparedStatement pstmt = null;
		String query = "UPDATE events SET startDate = ? , endDate = ? , topic = ? , participants = ? WHERE userid_ref = ? AND id = ?";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setLong(1, startDate);
			pstmt.setLong(2, endDate);
			pstmt.setString(3, topic);
			pstmt.setString(4, participants);
			pstmt.setString(5, userId);
			pstmt.setInt(6, eventId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Deletes an event from the database
	 * 
	 * @param eventId
	 *            is the id of the event to delete in the database
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int deleteEvent(String eventId) {
		String query = "DELETE from events WHERE id = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, eventId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Finds an event in the database by a given id and user id
	 * 
	 * @param eventId
	 *            is the id of the event
	 * @param userId
	 *            is the id of the user
	 * @return the {@link EventEntity} or null if the event wasn't found
	 */
	public EventEntity findEvent(String eventId, String userId) {
		String query = "SELECT * FROM events WHERE id = ? AND userid_ref = ? ";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, eventId);
			pstmt.setString(2, userId);
			ResultSet rs = pstmt.executeQuery();
			EventEntity e = null;
			if (rs.next()) {
				e = new EventEntity(rs.getInt(1), rs.getString(6), rs.getString(4), rs.getLong(2), rs.getLong(3),
						rs.getString(5));
			}
			return e;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Finds all events from the db
	 * 
	 * @return a list of {@link EventEntity Event Entities} representing all
	 *         events in the database
	 */
	public List<EventEntity> findAllEvents() {
		String query = "SELECT * FROM events";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			List<EventEntity> events = new LinkedList<EventEntity>();
			while (rs.next()) {
				EventEntity e = new EventEntity(rs.getInt(1), rs.getString(6), rs.getString(4), rs.getLong(2),
						rs.getLong(3), rs.getString(5));
				events.add(e);
			}
			return events;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return new LinkedList<EventEntity>();
		}
	}

	/**
	 * Finds all events by a given user
	 * 
	 * @param userId
	 *            is the id of the user
	 * @return a list of {@link EventEntity Event Entities} representing all
	 *         events of a user
	 */
	public List<EventEntity> findAllEventsByUser(String userId) {
		String query = "SELECT * FROM events WHERE userid_ref = ? ";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, userId);
			ResultSet rs = pstmt.executeQuery();
			List<EventEntity> events = new LinkedList<EventEntity>();
			while (rs.next()) {
				EventEntity e = new EventEntity(rs.getInt(1), rs.getString(6), rs.getString(4), rs.getLong(2),
						rs.getLong(3), rs.getString(5));
				events.add(e);
			}
			return events;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return new LinkedList<EventEntity>();
		}
	}

	/**
	 * Inserts a todo into the database
	 * 
	 * @param topic
	 *            is the topic of the todo
	 * @param userId
	 *            is the id of the user who creates this todo
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int insertTodo(String topic, String userId) {
		PreparedStatement pstmt = null;
		String query = "INSERT into todos (userid_ref, topic)  VALUES(?,?)";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, userId);
			pstmt.setString(2, topic);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Updates a todo in the database
	 * 
	 * @param todoId
	 *            is the id of the todo
	 * @param topic
	 *            is the new topic of the todo
	 * @param userId
	 *            is the id of the user owning this todo
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int updateTodo(int todoId, String topic, String userId) {
		PreparedStatement pstmt = null;
		String query = "UPDATE todos SET topic = ? WHERE userid_ref = ? AND id = ?";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, topic);
			pstmt.setString(2, userId);
			pstmt.setInt(3, todoId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Deletes a todo from the database
	 * 
	 * @param todoId
	 *            is the id of the todo to delete in the database
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int deleteTodo(int todoId) {
		String query = "DELETE from todos WHERE id = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setInt(1, todoId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Finds a todo in the database by a given id and user id
	 * 
	 * @param todoId
	 *            is the id of the todo
	 * @param userId
	 *            is the id of the user
	 * @return the {@link TodoEntity} or null if the todo wasn't found
	 */
	public TodoEntity findTodo(int todoId, String userId) {
		String query = "SELECT * FROM todos WHERE id = ? AND userid_ref = ? ";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setInt(1, todoId);
			pstmt.setString(2, userId);
			ResultSet rs = pstmt.executeQuery();
			TodoEntity t = null;
			if (rs.next()) {
				t = new TodoEntity(rs.getInt(1), rs.getString(3), rs.getString(2));
			}
			return t;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Finds all todos from the db
	 * 
	 * @return a list of {@link TodoEntity Todo Entities} representing all todos
	 *         in the database
	 */
	public List<TodoEntity> findAllTodos() {
		String query = "SELECT * FROM todos";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			List<TodoEntity> todos = new LinkedList<TodoEntity>();
			while (rs.next()) {
				TodoEntity t = new TodoEntity(rs.getInt(1), rs.getString(2), rs.getString(3));
				todos.add(t);
			}
			return todos;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return new LinkedList<TodoEntity>();
		}
	}

	/**
	 * Finds all todos by a given user
	 * 
	 * @param userId
	 *            is the id of the user
	 * @return a list of {@link TodoEntity Todo Entities} representing all todos
	 *         of a user
	 */
	public List<TodoEntity> findAllTodosByUser(String userId) {
		String query = "SELECT * FROM todos WHERE userid_ref = ? ";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, userId);
			ResultSet rs = pstmt.executeQuery();
			List<TodoEntity> todos = new LinkedList<TodoEntity>();
			while (rs.next()) {
				TodoEntity t = new TodoEntity(rs.getInt(1), rs.getString(3), rs.getString(2));
				todos.add(t);
			}
			return todos;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return new LinkedList<TodoEntity>();
		}
	}

	/**
	 * Inserts a status into the database
	 * 
	 * @param name
	 *            is the name of the status
	 * @param time
	 *            is the time the user stayed in this status
	 * @param userId
	 *            is the id of the user
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int insertStatus(String name, long time, String userId) {
		PreparedStatement pstmt = null;
		Statement statement = null;
		String query = "INSERT into status (name)  VALUES(?)";
		try {
			connection.setAutoCommit(false);
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, name);
			int inserts = pstmt.executeUpdate();
			int generatedKey = 0;
			statement = connection.createStatement();
			ResultSet generatedKeys = statement.executeQuery("SELECT last_insert_rowid()");
			if (inserts == 1 && generatedKeys.next()) {
				generatedKey = generatedKeys.getInt(1);
			} else {
				return -1;
			}
			query = "INSERT into statususers (time, statusid_ref, userid_ref) VALUES (?,?,?)";
			pstmt = connection.prepareStatement(query);
			pstmt.setLong(1, time);
			pstmt.setInt(2, generatedKey);
			pstmt.setString(3, userId);
			pstmt.executeUpdate();
			connection.commit();
			connection.setAutoCommit(true);
			return inserts;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Updates a status in the database
	 * 
	 * @param statusId
	 *            is the id of the status
	 * @param name
	 *            is the new name of the status
	 * @param time
	 *            is the time the user stayed in the status
	 * @param userId
	 *            is the id of the user
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	public int updateStatus(int statusId, String name, long time, String userId) {
		PreparedStatement pstmt = null;
		String query = "UPDATE status SET name = ? WHERE id = ?";
		try {
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, name);
			pstmt.setInt(2, statusId);
			int updates = pstmt.executeUpdate();
			query = "UPDATE statususers SET time = ? WHERE statusid_ref = ? AND userid_ref = ?";
			pstmt = connection.prepareStatement(query);
			pstmt.setLong(1, time);
			pstmt.setInt(2, statusId);
			pstmt.setString(3, userId);
			pstmt.executeUpdate();
			return updates;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Deletes a status from the database
	 * 
	 * @param userId
	 *            is the id of the user of the status
	 * @return an integer greater than -1 for success, otherwise -1
	 */
	private int deleteStatus(String userId) {
		String query = "SELECT * from statususers WHERE userid_ref = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt = connection.prepareStatement(query);
			pstmt.setString(1, userId);
			ResultSet rs = pstmt.executeQuery();
			query = "DELETE from statususers WHERE userid_ref = ?";
			pstmt = connection.prepareStatement(query);
			int deleted = pstmt.executeUpdate();
			query = "DELETE from status WHERE id = ?";
			pstmt = connection.prepareStatement(query);
			while (rs.next()) {
				int statusId = rs.getInt(3);
				if (findStatus(statusId) == null) {
					pstmt.setInt(1, statusId);
					pstmt.executeUpdate();
				}
			}
			return deleted;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return -1;
		}
	}

	/**
	 * Finds a status in the database by a given id and user id
	 * 
	 * @param statusId
	 *            is the id of the status
	 * @param userId
	 *            is the id of the user
	 * @return the {@link StatusEntity} or null if the status wasn't found
	 */
	private StatusEntity findStatus(int statusId) {
		String query = "SELECT * FROM statususers WHERE statusid_ref = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setInt(1, statusId);
			ResultSet rs = pstmt.executeQuery();
			StatusEntity s = null;
			if (rs.next()) {
				String name = findStatusName(statusId + "");
				s = new StatusEntity(statusId + "", name, rs.getLong(3), "");
			}
			return s;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Finds a status in the database by a given id and user id
	 * 
	 * @param statusId
	 *            is the id of the status
	 * @param userId
	 *            is the id of the user
	 * @return the {@link StatusEntity} or null if the status wasn't found
	 */
	public StatusEntity findStatus(int statusId, String userId) {
		String query = "SELECT * FROM statususers WHERE statusid_ref = ? AND userid_ref = ? ";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setInt(1, statusId);
			pstmt.setString(2, userId);
			ResultSet rs = pstmt.executeQuery();
			StatusEntity s = null;
			if (rs.next()) {
				String name = findStatusName(statusId + "");
				s = new StatusEntity(statusId + "", name, rs.getLong(3), userId);
			}
			return s;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Finds all status from the db
	 * 
	 * @return a list of {@link StatusEntity Status Entities} representing all
	 *         status in the database
	 */
	public List<StatusEntity> findAllStatus() {
		String query = "SELECT * FROM status";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			List<StatusEntity> status = new LinkedList<StatusEntity>();
			while (rs.next()) {
				StatusEntity s = new StatusEntity(rs.getInt(1) + "", rs.getString(2), 0, "");
				status.add(s);
			}
			return status;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return new LinkedList<StatusEntity>();
		}
	}

	/**
	 * Finds all status by a given user
	 * 
	 * @param userId
	 *            is the id of the user
	 * @return a list of {@link StatusEntity Status Entities} representing all
	 *         status of a user
	 */
	public List<StatusEntity> findAllStatusByUser(String userId) {
		String query = "SELECT * FROM statususers WHERE userid_ref = ? ";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, userId);
			ResultSet rs = pstmt.executeQuery();
			List<StatusEntity> status = new LinkedList<StatusEntity>();
			while (rs.next()) {
				long time = rs.getLong(2);
				int statusId = rs.getInt(3);
				String name = findStatusName(statusId + "");
				StatusEntity s = new StatusEntity(statusId + "", name, time, userId);
				status.add(s);
			}
			return status;
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return new LinkedList<StatusEntity>();
		}
	}

	/**
	 * Finds a name of a status from the db
	 * 
	 * @param statusId
	 *            is the id of the status
	 * @return a string containing the name of the status
	 */
	private String findStatusName(String statusId) {
		String query = "SELECT name FROM status WHERE id = ?";
		try {
			PreparedStatement pstmt = connection.prepareStatement(query);
			pstmt.setString(1, statusId);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			} else {
				return null;
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}
}
