/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package de.hadesrofl.jimmey.utils;

import sx.blah.discord.handle.obj.IUser;

/**
 * 
 * 
 * <p>
 * <strong>last update:</strong> 13.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * A class to represent a reminder object used in
 * {@link de.hadesrofl.jimmey.task.ReminderTask ReminderTask}
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.15
 */
public class Reminder implements Comparable<Reminder> {
	/**
	 * Topic of the reminder
	 */
	private String topic;
	/**
	 * User who set the reminder
	 */
	private IUser user;
	/**
	 * Time to trigger the reminder in seconds of the day
	 */
	private long timeToFire;

	/**
	 * Constructor
	 * 
	 * @param user
	 *            is the user who set the reminder
	 * @param topic
	 *            is the topic to remind to
	 * @param timeToFire
	 *            is the time to trigger the reminder in seconds of the day
	 */
	public Reminder(IUser user, String topic, long timeToFire) {
		this.topic = topic;
		this.user = user;
		this.timeToFire = timeToFire;
	}

	/**
	 * Gets the topic
	 * 
	 * @return the topic as String
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * Gets the user
	 * 
	 * @return the user as IUser
	 */
	public IUser getUser() {
		return user;
	}

	/**
	 * Gets the timeToFire
	 * 
	 * @return the timeToFire as long
	 */
	public long getTimeToFire() {
		return timeToFire;
	}

	/**
	 * Compares two Reminder objects.
	 * 
	 * @return 0 if the objects are equal, if this.timeToFire is greater than o.timeToFire it
	 *         returns 1, otherwise if this.timeToFire is less than o.timeToFire it returns
	 *         -1
	 */
	public int compareTo(Reminder o) {
		if (this.timeToFire > o.getTimeToFire()) {
			return 1;
		} else if (this.timeToFire < o.getTimeToFire()) {
			return -1;
		} else {
			return 0;
		}
	}

}
