/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey.utils;

import java.util.List;
import java.util.Optional;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IPrivateChannel;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.handle.obj.Presences;
import sx.blah.discord.handle.obj.Status;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * 
 * 
 * <strong>last update:</strong> 23.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * User class used in tests
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.21
 */
public class TUser implements IUser {
	/**
	 * Id of this test user
	 */
	private String id;
	/**
	 * Is the client used by this test user
	 */
	private IDiscordClient client;
	/**
	 * Name of the user
	 */
	private String name;

	/**
	 * Constructor
	 * 
	 * @param id
	 *            is the test user's id
	 * @param client
	 *            is the client used by this test user
	 * @param name
	 *            is the name of the user
	 */
	public TUser(String id, String name, IDiscordClient client) {
		this.id = id;
		this.client = client;
	}

	@Override
	public String getID() {
		return this.id;
	}

	@Override
	public IDiscordClient getClient() {
		return client;
	}

	@Override
	public IUser copy() {
		return null;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Status getStatus() {
		return null;
	}

	@Override
	public String getAvatar() {
		return null;
	}

	@Override
	public String getAvatarURL() {
		return null;
	}

	@Override
	public Presences getPresence() {
		return null;
	}

	@Override
	public String getDisplayName(IGuild guild) {
		return null;
	}

	@Override
	public String mention() {
		return null;
	}

	@Override
	public String mention(boolean mentionWithNickname) {
		return null;
	}

	@Override
	public String getDiscriminator() {
		return null;
	}

	@Override
	public List<IRole> getRolesForGuild(IGuild guild) {
		return null;
	}

	@Override
	public Optional<String> getNicknameForGuild(IGuild guild) {
		return null;
	}

	@Override
	public boolean isBot() {
		return false;
	}

	@Override
	public void moveToVoiceChannel(IVoiceChannel newChannel)
			throws DiscordException, RateLimitException, MissingPermissionsException {

	}

	@Override
	public List<IVoiceChannel> getConnectedVoiceChannels() {
		return null;
	}

	@Override
	public IPrivateChannel getOrCreatePMChannel() throws RateLimitException, DiscordException {
		return client.getOrCreatePMChannel(this);
	}

	@Override
	public boolean isDeaf(IGuild guild) {
		return false;
	}

	@Override
	public boolean isMuted(IGuild guild) {
		return false;
	}

	@Override
	public boolean isDeafLocally() {
		return false;
	}

	@Override
	public boolean isMutedLocally() {
		return false;
	}

	@Override
	public void addRole(IRole role) throws MissingPermissionsException, RateLimitException, DiscordException {

	}

	@Override
	public void removeRole(IRole role) throws MissingPermissionsException, RateLimitException, DiscordException {

	}

}
