package de.hadesrofl.jimmey.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.audio.IAudioManager;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IInvite;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRegion;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.Image;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

public class TGuild implements IGuild {
	private String id;
	private IDiscordClient client;
	private IUser owner;
	private List<IChannel> channels;

	public TGuild(String id, IUser owner, IDiscordClient client, List<IChannel> channels) {
		this.id = id;
		this.owner = owner;
		this.client = client;
		this.channels = channels;
	}

	@Override
	public String getID() {
		return id;
	}

	@Override
	public IDiscordClient getClient() {
		return client;
	}

	@Override
	public IGuild copy() {
		return null;
	}

	@Override
	public String getOwnerID() {
		return "15";
	}

	@Override
	public IUser getOwner() {
		return owner;
	}

	@Override
	public String getIcon() {
		return null;
	}

	@Override
	public String getIconURL() {
		return null;
	}

	@Override
	public List<IChannel> getChannels() {
		return channels;
	}

	@Override
	public IChannel getChannelByID(String id) {
		return null;
	}

	@Override
	public List<IUser> getUsers() {
		List<IUser> users = new ArrayList<IUser>();
		users.add(owner);
		return users;
	}

	@Override
	public IUser getUserByID(String id) {
		return null;
	}

	@Override
	public List<IChannel> getChannelsByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<IVoiceChannel> getVoiceChannelsByName(String name) {
		return null;
	}

	@Override
	public List<IUser> getUsersByName(String name) {
		return null;
	}

	@Override
	public List<IUser> getUsersByName(String name, boolean includeNicknames) {
		return null;
	}

	@Override
	public String getName() {
		return "Test Guild";
	}

	@Override
	public List<IRole> getRoles() {
		return new ArrayList<IRole>();
	}

	@Override
	public List<IRole> getRolesForUser(IUser user) {
		return null;
	}

	@Override
	public IRole getRoleByID(String id) {
		return null;
	}

	@Override
	public List<IRole> getRolesByName(String name) {
		return null;
	}

	@Override
	public List<IVoiceChannel> getVoiceChannels() {
		return new ArrayList<IVoiceChannel>();
	}

	@Override
	public IVoiceChannel getVoiceChannelByID(String id) {
		return null;
	}

	@Override
	public IVoiceChannel getAFKChannel() {
		return null;
	}

	@Override
	public int getAFKTimeout() {
		return 0;
	}

	@Override
	public IRole createRole() throws MissingPermissionsException, RateLimitException, DiscordException {
		return null;
	}

	@Override
	public List<IUser> getBannedUsers() throws RateLimitException, DiscordException {
		return new ArrayList<IUser>();
	}

	@Override
	public void banUser(IUser user) throws MissingPermissionsException, RateLimitException, DiscordException {
	}

	@Override
	public void banUser(IUser user, int deleteMessagesForDays)
			throws MissingPermissionsException, RateLimitException, DiscordException {
	}

	@Override
	public void pardonUser(String userID) throws MissingPermissionsException, RateLimitException, DiscordException {
	}

	@Override
	public void kickUser(IUser user) throws MissingPermissionsException, RateLimitException, DiscordException {
	}

	@Override
	public void editUserRoles(IUser user, IRole[] roles)
			throws MissingPermissionsException, RateLimitException, DiscordException {
	}

	@Override
	public void setDeafenUser(IUser user, boolean deafen)
			throws MissingPermissionsException, DiscordException, RateLimitException {
	}

	@Override
	public void setMuteUser(IUser user, boolean mute)
			throws DiscordException, RateLimitException, MissingPermissionsException {
	}

	@Override
	public void setUserNickname(IUser user, String nick)
			throws MissingPermissionsException, DiscordException, RateLimitException {
	}

	@Override
	public void changeName(String name) throws RateLimitException, DiscordException, MissingPermissionsException {
	}

	@Override
	public void changeRegion(IRegion region) throws RateLimitException, DiscordException, MissingPermissionsException {
	}

	@Override
	public void changeIcon(Image icon) throws RateLimitException, DiscordException, MissingPermissionsException {
	}

	@Override
	public void changeAFKChannel(IVoiceChannel channel)
			throws RateLimitException, DiscordException, MissingPermissionsException {
	}

	@Override
	public void changeAFKTimeout(int timeout) throws RateLimitException, DiscordException, MissingPermissionsException {
	}

	@Override
	public void deleteGuild() throws DiscordException, RateLimitException, MissingPermissionsException {
	}

	@Override
	public void leaveGuild() throws DiscordException, RateLimitException {
	}

	@Override
	public IChannel createChannel(String name)
			throws DiscordException, MissingPermissionsException, RateLimitException {
		return null;
	}

	@Override
	public IVoiceChannel createVoiceChannel(String name)
			throws DiscordException, MissingPermissionsException, RateLimitException {
		return null;
	}

	@Override
	public IRegion getRegion() {
		return new TRegion();
	}

	@Override
	public void transferOwnership(IUser newOwner)
			throws RateLimitException, MissingPermissionsException, DiscordException {
	}

	@Override
	public IRole getEveryoneRole() {
		return null;
	}

	@Override
	public List<IInvite> getInvites() throws DiscordException, RateLimitException, MissingPermissionsException {
		return new ArrayList<IInvite>();
	}

	@Override
	public void reorderRoles(IRole... rolesInOrder)
			throws DiscordException, RateLimitException, MissingPermissionsException {
	}

	@Override
	public int getUsersToBePruned(int days) throws DiscordException, RateLimitException {
		return 0;
	}

	@Override
	public int pruneUsers(int days) throws DiscordException, RateLimitException {
		return 0;
	}

	@Override
	public void addBot(String applicationID, EnumSet<Permissions> permissions)
			throws MissingPermissionsException, DiscordException, RateLimitException {
	}

	@Override
	public IAudioManager getAudioManager() {
		return null;
	}

	@Override
	public LocalDateTime getJoinTimeForUser(IUser user) throws DiscordException {
		return null;
	}

	@Override
	public IMessage getMessageByID(String id) {
		return null;
	}

}
