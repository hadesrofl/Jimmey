package de.hadesrofl.jimmey.utils;

import sx.blah.discord.handle.obj.IRegion;

public class TRegion implements IRegion {

	@Override
	public String getID() {
		return "152132134";
	}

	@Override
	public String getName() {
		return "Frankfurt";
	}

	@Override
	public boolean isVIPOnly() {
		return false;
	}

}
