/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package de.hadesrofl.jimmey.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * 
 * 
 * <p>
 * <strong>last update:</strong> 23.11.2016
 * </p>
 * <b>Description:</b>
 * <p>
 * Message class for test cases
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.21
 */
public class TMessage implements IMessage {
	/**
	 * Content of the message
	 */
	private String content;
	/**
	 * Channel where the message was posted
	 */
	private IChannel channel;
	/**
	 * Client used for this test message
	 */
	private IDiscordClient client;
	/**
	 * Author of this test message
	 */
	private IUser author;
	/**
	 * Guild of this message
	 */
	private IGuild guild;

	/**
	 * Constructor
	 * 
	 * @param content
	 *            is the content of the message
	 * @param channel
	 *            is the channel where it belongs to
	 * @param client
	 *            is the client the message was sent by
	 * @param author
	 *            is the author of this message
	 * @param guild
	 *            is the IGuild of this message
	 */
	public TMessage(String content, IChannel channel, IDiscordClient client, IUser author, IGuild guild) {
		this.content = content;
		this.channel = channel;
		this.client = client;
		this.author = author;
		this.guild = guild;
	}

	@Override
	public String getID() {
		return null;
	}

	@Override
	public IDiscordClient getClient() {
		return client;
	}

	@Override
	public IMessage copy() {
		return null;
	}

	@Override
	public String getContent() {
		return this.content;
	}

	@Override
	public IChannel getChannel() {
		return this.channel;
	}

	@Override
	public IUser getAuthor() {
		return author;
	}

	@Override
	public LocalDateTime getTimestamp() {
		return null;
	}

	@Override
	public List<IUser> getMentions() {
		return new ArrayList<IUser>();
	}

	@Override
	public List<IRole> getRoleMentions() {
		return null;
	}

	@Override
	public List<IChannel> getChannelMentions() {
		return null;
	}

	@Override
	public List<Attachment> getAttachments() {
		return null;
	}

	@Override
	public List<IEmbedded> getEmbedded() {
		return null;
	}

	@Override
	public void reply(String content) throws MissingPermissionsException, RateLimitException, DiscordException {

	}

	@Override
	public IMessage edit(String content) throws MissingPermissionsException, RateLimitException, DiscordException {
		return null;
	}

	@Override
	public boolean mentionsEveryone() {
		return false;
	}

	@Override
	public void delete() throws MissingPermissionsException, RateLimitException, DiscordException {

	}

	@Override
	public Optional<LocalDateTime> getEditedTimestamp() {
		return null;
	}

	@Override
	public boolean isPinned() {
		return false;
	}

	@Override
	public IGuild getGuild() {
		return guild;
	}

}
