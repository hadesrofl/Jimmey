/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package de.hadesrofl.jimmey.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IInvite;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MessageList;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * 
 * 
 * <p>
 * <strong>last update:</strong> 23.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * Channel class for testcases
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.21
 */
public class TChannel implements IChannel {
	/**
	 * Id of the channel
	 */
	private String id;
	/**
	 * client connected to this channel
	 */
	private IDiscordClient client;

	/**
	 * Constructor
	 * 
	 * @param id
	 *            is the id of the channel
	 * @param client
	 *            is the client connected to the channel
	 */
	public TChannel(String id, IDiscordClient client) {
		this.id = id;
		this.client = client;
	}

	@Override
	public String getID() {
		return this.id;
	}

	@Override
	public IDiscordClient getClient() {
		return this.client;
	}

	@Override
	public IChannel copy() {
		return null;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public MessageList getMessages() {
		return null;
	}

	@Override
	public IMessage getMessageByID(String messageID) {
		return null;
	}

	@Override
	public IGuild getGuild() {
		return null;
	}

	@Override
	public boolean isPrivate() {
		return false;
	}

	@Override
	public String getTopic() {
		return null;
	}

	@Override
	public String mention() {
		return null;
	}

	@Override
	public IMessage sendMessage(String content)
			throws MissingPermissionsException, RateLimitException, DiscordException {
		return sendMessage(content, false);
	}

	@Override
	public IMessage sendMessage(String content, boolean tts)
			throws MissingPermissionsException, RateLimitException, DiscordException {
		return null;
	}

	@Override
	public IMessage sendFile(File file, String content)
			throws IOException, MissingPermissionsException, RateLimitException, DiscordException {
		return null;
	}

	@Override
	public IMessage sendFile(File file)
			throws IOException, MissingPermissionsException, RateLimitException, DiscordException {
		return null;
	}

	@Override
	public IMessage sendFile(InputStream stream, String filename, String content)
			throws IOException, MissingPermissionsException, RateLimitException, DiscordException {
		return null;
	}

	@Override
	public IMessage sendFile(InputStream stream, String filename)
			throws IOException, MissingPermissionsException, RateLimitException, DiscordException {
		return null;
	}

	@Override
	public IInvite createInvite(int maxAge, int maxUses, boolean temporary)
			throws MissingPermissionsException, RateLimitException, DiscordException {
		return null;
	}

	@Override
	public void toggleTypingStatus() {

	}

	@Override
	public void setTypingStatus(boolean typing) {

	}

	@Override
	public boolean getTypingStatus() {
		return false;
	}

	@Override
	public void changeName(String name) throws RateLimitException, DiscordException, MissingPermissionsException {

	}

	@Override
	public void changePosition(int position) throws RateLimitException, DiscordException, MissingPermissionsException {

	}

	@Override
	public void changeTopic(String topic) throws RateLimitException, DiscordException, MissingPermissionsException {

	}

	@Override
	public int getPosition() {
		return 0;
	}

	@Override
	public void delete() throws MissingPermissionsException, RateLimitException, DiscordException {

	}

	@Override
	public Map<String, PermissionOverride> getUserOverrides() {
		return null;
	}

	@Override
	public Map<String, PermissionOverride> getRoleOverrides() {
		return null;
	}

	@Override
	public EnumSet<Permissions> getModifiedPermissions(IUser user) {
		return null;
	}

	@Override
	public EnumSet<Permissions> getModifiedPermissions(IRole role) {
		return null;
	}

	@Override
	public void removePermissionsOverride(IUser user)
			throws MissingPermissionsException, RateLimitException, DiscordException {

	}

	@Override
	public void removePermissionsOverride(IRole role)
			throws MissingPermissionsException, RateLimitException, DiscordException {

	}

	@Override
	public void overrideRolePermissions(IRole role, EnumSet<Permissions> toAdd, EnumSet<Permissions> toRemove)
			throws MissingPermissionsException, RateLimitException, DiscordException {

	}

	@Override
	public void overrideUserPermissions(IUser user, EnumSet<Permissions> toAdd, EnumSet<Permissions> toRemove)
			throws MissingPermissionsException, RateLimitException, DiscordException {

	}

	@Override
	public List<IInvite> getInvites() throws DiscordException, RateLimitException, MissingPermissionsException {
		return null;
	}

	@Override
	public List<IUser> getUsersHere() {
		return null;
	}

	@Override
	public List<IMessage> getPinnedMessages() throws RateLimitException, DiscordException {
		return null;
	}

	@Override
	public void pin(IMessage message) throws RateLimitException, DiscordException, MissingPermissionsException {

	}

	@Override
	public void unpin(IMessage message) throws RateLimitException, DiscordException, MissingPermissionsException {

	}

}
