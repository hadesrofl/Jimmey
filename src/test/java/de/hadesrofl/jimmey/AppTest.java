/**
 * Jimmey - a discord bot written with thanks to Discord4J (https://github.com/austinv11/Discord4J)
 * Copyright (c) 2016 René Kremer
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.hadesrofl.jimmey;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.junit.Before;

import de.hadesrofl.jimmey.bot.BaseBot;
import de.hadesrofl.jimmey.bot.JimmeyBot;
import de.hadesrofl.jimmey.enums.CommandEnum;
import de.hadesrofl.jimmey.utils.JsonReader;
import de.hadesrofl.jimmey.utils.TChannel;
import de.hadesrofl.jimmey.utils.TGuild;
import de.hadesrofl.jimmey.utils.TMessage;
import de.hadesrofl.jimmey.utils.TUser;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.MentionEvent;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.RateLimitException;

/**
 * 
 * <p>
 * <strong>last update:</strong> 27.11.2016
 * </p>
 * <strong>Description:</strong>
 * <p>
 * Test class with test cases for jimmey
 * </p>
 * 
 * @author Rene Kremer
 *         <p>
 *         Copyright (c) 2016 by Rene Kremer
 *         </p>
 *         <p>
 *         <strong>License:</strong> GPL-2.0
 *         </p>
 * @version 0.25
 */
public class AppTest extends TestCase {
	/**
	 * Token used for connecting to a client
	 */
	private String TOKEN;
	/**
	 * Bot object of jimmey
	 */
	private JimmeyBot bot;
	/**
	 * Client object to which jimmey connects
	 */
	private IDiscordClient client;
	/**
	 * Prefix for commands
	 */
	private String prefix;

	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
		setup();
	}

	/**
	 * Sets up a connection to a client and therefore jimmey
	 */
	@Before
	public void setup() {
		String username = "";
		String timezone = "";
		String avatarFile = "";
		long reminderDelay = 0;
		long reminderPeriod = 0;
		long CET = 0;
		String iCalOutput = "";
		try {
			JSONObject botConfig = JsonReader.readFile("resources/config.json").getJSONObject("bot-config");
			TOKEN = botConfig.getString("token");
			prefix = botConfig.getString("prefix");
			username = botConfig.getString("username");
			timezone = botConfig.getString("timezone");
			reminderDelay = botConfig.getJSONObject("reminder").getLong("delay");
			reminderPeriod = botConfig.getJSONObject("reminder").getLong("period");
			CET = botConfig.getLong("cleanerExecutionTime");
			avatarFile = botConfig.getString("avatar");
			iCalOutput = botConfig.getString("ICalOutput");
		} catch (Exception e) {
			System.err.println("An error occured while loading the config.json...");
			TOKEN = System.getProperty("token");
			username = "Jimmey";
			timezone = "+01:00";
			reminderDelay = 30000;
			reminderPeriod = 60000;
			CET = 5000;
			avatarFile = "resources/avatars/book.png";
			iCalOutput = "resources/tmpICal.ics";
		}
		client = BaseBot.login(TOKEN);
		bot = new JimmeyBot(client, prefix, username, timezone, reminderDelay, reminderPeriod, CET, avatarFile,
				iCalOutput);
		BotLauncher.INSTANCE = bot;
		for (CommandEnum e : CommandEnum.values()) {
			e.updateBot();
		}
	}

	/**
	 * Sleep method for milliseconds
	 * 
	 * @param millis
	 *            is the time to sleep
	 */
	public void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	/**
	 * Tests for ReadyEvents, MessageReceivedEvents and MentionEvents TODO: More
	 * cases
	 */
	public void testApp() {
		TChannel ch = new TChannel("245200386799960065", client);
		TUser user = new TUser("195938166996336650", "test user", client);
		List<IChannel> channels = new ArrayList<IChannel>();
		channels.add(ch);
		TGuild guild = new TGuild("195549180587016192", user, client, channels);
		TMessage message = new TMessage("Test", ch, client, user, guild);
		// Wait for established connection
		sleep(5000);
		readyTest(bot);
		mentionTest(bot, message);
		message = new TMessage(prefix + "games", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "games -show", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "games -help", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "help", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "remind -help", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "remind topic 11:30", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "remind topic 11:30 +01:00", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "schedule gaming 2016-11-07T18:00 Bob", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "schedule gaming 2016-11-07T18:00", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "schedule -edit 1 Bla 2016-11-07T18:00 Bob", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "schedule -remove 1", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "schedule gaming", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "schedule -show", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "schedule -help", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "stats -help", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "stats", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "time", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "time -help", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "time +01:00", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "time -help +01:00", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "todo Stuff", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "todo", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "todo -help", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "todo -show", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "todo -edit 1 Test", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		message = new TMessage(prefix + "todo -remove 1", ch, client, user, guild);
		commandTest(bot, message);
		sleep(2000);
		try {
			bot.getClient().logout();
		} catch (RateLimitException | DiscordException e1) {
			e1.printStackTrace();
		}
		String a[] = { TOKEN };
		BotLauncher.main(a);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test for MentionEvents
	 * 
	 * @param bot
	 *            is the bot that shall be tested
	 * @param message
	 *            is the message to test the bot with
	 */
	public void mentionTest(JimmeyBot bot, IMessage message) {
		bot.onMentionEvent(new MentionEvent(message));
	}

	/**
	 * Test for commands and therefore MessageReceivedEvents
	 * 
	 * @param bot
	 *            is the bot that shall be tested
	 * @param message
	 *            is the message to test the bot with
	 */
	public void commandTest(JimmeyBot bot, IMessage message) {
		bot.onMessageReceivedEvent(new MessageReceivedEvent(message));
	}

	/**
	 * Test for ReadyEvents
	 * 
	 * @param bot
	 *            is the bot that shall be tested
	 */
	public void readyTest(JimmeyBot bot) {
		bot.onReady(new ReadyEvent());
	}
}
